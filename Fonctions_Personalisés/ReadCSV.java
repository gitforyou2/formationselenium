package main.java.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

import org.junit.Assert;

import com.opencsv.CSVReader;

import main.java.constantes.Constantes;

public class ReadCSV  extends ApplicationCommonScripts{

	private CSVReader reader;
	private String nomFichier;

	public ReadCSV(){}

	/**
	 * Lire le fichier CSV
	 * @param file
	 */
	public void lire(String file)
	{
		try {
			FileInputStream fis = new FileInputStream(LOGGER.dossierLogExec.getPath() + "\\" + file);
			InputStreamReader isr = new InputStreamReader(fis,StandardCharsets.UTF_8);
			reader = new CSVReader(isr);

		} catch (FileNotFoundException e) {
			LOGGER.error("Impossible d'ouvrir le fichier: " + nomFichier);
			e.printStackTrace();
		}
	}

	/**
	 * Verifier la présence du Habilitation I2A dans le fichier excel
	 * @param nomColonne, doitEtrePresent
	 */

	public void VerifierPresenceHabilitationI2A(String nomColonne, boolean doitEtrePresent)
	{
		String [] nextLine;
		String ligne[] = null;
		boolean nomColonneTrouve = false;
		boolean valueColonneTrouve = false;
		try {	

			while ((nextLine = reader.readNext()) != null) {
				ligne = nextLine[0].split(";");
				
				if(ligne.length > 1)
				{
					if(ligne[ligne.length-2].contains(nomColonne))
					{
						nomColonneTrouve = true;
					}
					
					if(ligne[ligne.length-2].contains("Aucun") || ligne[ligne.length-2].contains("En erreur") || ligne[ligne.length-2].contains("Effective"))
					{
						valueColonneTrouve = true;
					}
				}
			}

			if(doitEtrePresent)
			{
				if(nomColonneTrouve && valueColonneTrouve)
				{
					LOGGER.info("La colonne Habilitation I2A est bien présent dans le fichier CSV");
					Assert.assertTrue(true);
				}
				else
				{
					LOGGER.error("La colonne Habilitation I2A est absent du fichier CSV");
					Assert.assertTrue(false);
				}
			}
			else 
				if(nomColonneTrouve)
				{
					LOGGER.error("La colonne Habilitation I2A est présent dans le fichier CSV");
					Assert.assertTrue(false);
				}
				else
				{
					LOGGER.info("La colonne Habilitation I2A est bien absent dans le fichier CSV");
					Assert.assertTrue(true);
				}

		} catch (IOException e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}
	

	}

	

	/**
	 * Verifier la présence du Socle dans le fichier excel
	 * @param nomColonne, doitEtrePresent
	 */

	public void VerifierPresenceSocle(String nomColonne, boolean doitEtrePresent)
	{
		String [] nextLine;
		String ligne[] = null;
		boolean nomColonneTrouve = false;
		boolean valueColonneTrouve = false;
		try {	

			while ((nextLine = reader.readNext()) != null) {
				ligne = nextLine[0].split(";");
				
				if(ligne.length > 1)
				{
					if(ligne[3].contains(nomColonne))
					{
						nomColonneTrouve = true;
					}
					
					if(ligne[3].contains("Aucun") || ligne[3].contains("Google Apps Device"))
					{
						valueColonneTrouve = true;
					}
				}
			}

			if(doitEtrePresent)
			{
				if(nomColonneTrouve && valueColonneTrouve)
				{
					LOGGER.info("La colonne socle est bien présent dans le fichier CSV");
					Assert.assertTrue(true);
				}
				else
				{
					LOGGER.error("La colonne SOCLE est absent du fichier CSV");
					Assert.assertTrue(false);
				}
			}
			else 
				if(nomColonneTrouve)
				{
					LOGGER.error("La colonne SOCLE est présent dans le fichier CSV");
					Assert.assertTrue(false);
				}
				else
				{
					LOGGER.info("La colonne SOCLE est bien absent dans le fichier CSV");
					Assert.assertTrue(true);
				}

		} catch (IOException e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}
	

	}
	
	/**
	 * Verifier une colonne
	 * @param nomColonne, doitEtrePresent
	 * @throws FileNotFoundException 
	 */
	
	public void VerifierColonne(String nomColonne) throws FileNotFoundException
	{
		String [] nextLine;
		String ligne[] = null;
		boolean nomColonneTrouve = false;
		        
		try {	
			
			while ((nextLine = reader.readNext()) != null) {
				ligne = nextLine[0].split(";");
				
				if(ligne.length > 1)
				{
					for(String field: ligne){
						
						if(field.contains(nomColonne))
						{
							nomColonneTrouve = true;
						}
					}
										
				}
			}
			reader.close();
			if(nomColonneTrouve)
				{
					LOGGER.error("La colonne "+nomColonne+" est présente dans le fichier CSV");
					Assert.assertTrue(true);
				}
				else
				{
					LOGGER.info("La colonne "+nomColonne+" est bien absente dans le fichier CSV");
					Assert.assertTrue(false);
				}
			
		} catch (IOException e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}
		
		
	}


	/**
	 * Verifier la le code état du contrat dans le fichier excel
	 * @param numContrat
	 * @param codeEtat
	 */

	public void VerifierCodeEtat(String ligne[], String codeEtat)
	{
		try {	
			logPDT("Vérification du code état du contrat dans le fichier Excel");
			if(ligne[8].equals(codeEtat))
				LOGGER.info("Le code état est correct");
			else
			{
				LOGGER.error("Le code état est incorrect: " + ligne[8]);
				Assert.assertTrue(false);
			}


		} catch (Exception e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}
	}


	/**
	 * Verifier le statut du contrat dans le fichier excel
	 * @param numContrat
	 */

	public void VerifierStatutContrat(String ligne[], String statutContrat)
	{
		try {	
			logPDT("Vérification du Statut du contrat dans le fichier Excel");
			if(ligne[9].equals(statutContrat))
				LOGGER.info("Le Statut est correct");
			else
			{
				LOGGER.error("Le Statut est incorrect: " + ligne[9]);
				Assert.assertTrue(false);
			}


		} catch (Exception e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * Cette fonction permet de supprimer un fichier
	 * @param nomFichier
	 */
	public void supprimerFichiersCSV(){
		try{

			File[] fichiersASupprimer = new File(Constantes.pathTelechargement()).listFiles();

			for(int i = 0; i < fichiersASupprimer.length; i++)
			{
				if(fichiersASupprimer[i].getName().equals(nomFichier))
				{
					if(fichiersASupprimer[i].delete()){
						LOGGER.info("Le fichier - "+fichiersASupprimer[i].getName() + " - est supprimé!");
					}else{
						LOGGER.error("Le fichier n existe pas !.");
					}
				}
			}

		}catch(Exception e){

			e.printStackTrace();

		}

	}


	/**
	 * Cette fonction permet de déplacer un fichier
	 * @param nomFichier
	 */
	public void deplacerFichier(String nomFichier){
		try{

			File file = new File(Constantes.pathTelechargement()+nomFichier);

			file.renameTo(new File(LOGGER.dossierLogExec.getPath() + "\\" + file.getName()));
			LOGGER.info("Le fichier - "+file.getName() + " - a bien été déplacé!");


		}catch(Exception e){
			LOGGER.error("Le fichier n'a pas été déplacé !.");
			e.printStackTrace();

		}

	}
	/**
	 * Verifier l'absence d'une colonne dans le fichier excel
	 * @param nomColonne, doitEtreAbsent
	 */

	public void VerifierAbsenceColonne(String nomColonne, boolean doitEtreAbsent)
	{

		String [] nextLine;
		String ligne[] = null;
		boolean nomColonneTrouve = false;
		        
		try {	
			
			while ((nextLine = reader.readNext()) != null) {
				ligne = nextLine[0].split(";");
				
				if(ligne.length > 1)
					
				{
					for(String field: ligne){
						
						if(!field.contains(nomColonne))
						{
							nomColonneTrouve = true;
						}
					}
										
				}
			}
			reader.close();
			if(nomColonneTrouve)
				{
					LOGGER.error("La colonne "+nomColonne+" est absente dans le fichier CSV");
					Assert.assertTrue(true);
				}
				else
				{
					LOGGER.info("La colonne "+nomColonne+" est bien prèsente dans le fichier CSV");
					Assert.assertTrue(false);
				}
			
		} catch (IOException e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}
		
		
	}

	public void VerifierAbsenceColonne1(String nomColonne, boolean doitEtreAbsent)
	{

		String [] nextLine;
		String ligne[] = null;
		boolean nomColonneTrouve = false;
		        
		try {

			while ((nextLine = reader.readNext()) != null) {
				ligne = nextLine[0].split(";");

				if(ligne[0].length() > 1)
					
				{
//					for(String field: ligne){
						
						if(!ligne.equals(nomColonne))
						{
							nomColonneTrouve = true;
						}
//					}
										
				}
			}
			reader.close();
			if (nomColonneTrouve) {
				LOGGER.error("La colonne " + nomColonne + " est absente dans le fichier CSV");
				Assert.assertTrue(true);
			} else {
				LOGGER.info("La colonne " + nomColonne + " est bien prèsente dans le fichier CSV");
				Assert.assertTrue(false);
			}

		} catch (IOException e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}
		
		
	}


	}
