package main.java.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;
import main.java.constantes.Constantes;

@SuppressWarnings("rawtypes")
public class Log extends ApplicationCommonScripts{


	File dossierLogExec;
	File dossierLogTest;
	File logFile;
	File logFileInfo;
	File logFileWarn;
	File logFileDebug;
	File logFileError;
	File logFileStep;
	int niv;

	private final String sep = "==========";
	private final String tab = " -> ";
	protected static final Logger LOGGER = LoggerFactory.getLogger(ApplicationCommonScripts.class);


	public Log(String testName)
	{
		try
		{
			//On crée le dossier de log du cas de test s'il n'existe pas
			dossierLogExec = new File(Constantes.pathLogFolder + testName);

			if (!dossierLogExec.exists()) 
				dossierLogExec.mkdirs();

			//On supprime tous les fichiers déjà présents dans le dossier de logs (sans toucher aux dossiers)
			File[] dossierLog = new File(Constantes.pathLogFolder + "\\" + testName).listFiles();
			for (int i=0; i <dossierLog.length;i++) {
				if(dossierLog[i].isFile())
					dossierLog[i].delete();
			}

			//On crée le fichier de log
			logFile = new File(Constantes.pathLogFolder + dossierLogExec.getName() + "\\" + Constantes.pathLogFileName);
			if (!logFile.exists()) 
				logFile.createNewFile();
			//On crée le fichier de log Info
			logFileInfo = new File(Constantes.pathLogFolder + dossierLogExec.getName() + "\\Info_" + Constantes.pathLogFileName);
			if (!logFileInfo.exists()) 
				logFileInfo.createNewFile();
			//On crée le fichier de log Warn
			logFileWarn = new File(Constantes.pathLogFolder + dossierLogExec.getName() + "\\Warn_" + Constantes.pathLogFileName);
			if (!logFileWarn.exists()) 
				logFileWarn.createNewFile();
			//On crée le fichier de log Debug
			logFileDebug = new File(Constantes.pathLogFolder + dossierLogExec.getName() + "\\Debug_" + Constantes.pathLogFileName);
			if (!logFileDebug.exists()) 
				logFileDebug.createNewFile();
			//On crée le fichier de log Error
			logFileError = new File(Constantes.pathLogFolder + dossierLogExec.getName() + "\\Error_" + Constantes.pathLogFileName);
			if (!logFileError.exists()) 
				logFileError.createNewFile();
			//On crée le fichier de log Error
			logFileStep = new File(Constantes.pathLogFolder + dossierLogExec.getName() + "\\Step_" + Constantes.pathLogFileName);
			niv = 0;
			if (!logFileStep.exists()) 
				logFileStep.createNewFile();
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}

	}

	public Log(String className, String testName)
	{
		try
		{
			//On crée le dossier de log du cas de test s'il n'existe pas
			File dossierTestExec;
			dossierTestExec = new File(Constantes.pathLogFolder + className);

			if (!dossierTestExec.exists()) 
				dossierTestExec.mkdirs();

			dossierLogExec = new File(dossierTestExec.getPath() + "\\" + testName);

			if (!dossierLogExec.exists()) 
				dossierLogExec.mkdirs();


			//On supprime tous les fichiers déjà présents dans le dossier de logs (sans toucher aux dossiers)
			File[] dossierLog = new File(dossierLogExec.getPath()).listFiles();
			for (int i=0; i <dossierLog.length; i++) {
				if(dossierLog[i].isFile())
					dossierLog[i].delete();
			}

			//On crée le fichier de log
			logFile = new File(dossierLogExec.getPath() + "\\" + Constantes.pathLogFileName);
			if (!logFile.exists()) 
				logFile.createNewFile();
			//On crée le fichier de log Info
			logFileInfo = new File(dossierLogExec.getPath() + "\\Info_" + Constantes.pathLogFileName);
			if (!logFileInfo.exists()) 
				logFileInfo.createNewFile();
			//On crée le fichier de log Warn
			logFileWarn = new File(dossierLogExec.getPath() + "\\Warn_" + Constantes.pathLogFileName);
			if (!logFileWarn.exists()) 
				logFileWarn.createNewFile();
			//On crée le fichier de log Debug
			logFileDebug = new File(dossierLogExec.getPath() + "\\Debug_" + Constantes.pathLogFileName);
			if (!logFileDebug.exists()) 
				logFileDebug.createNewFile();
			//On crée le fichier de log Error
			logFileError = new File(dossierLogExec.getPath() + "\\Error_" + Constantes.pathLogFileName);
			if (!logFileError.exists()) 
				logFileError.createNewFile();		
			//On crée le fichier de log STEP
			logFileStep = new File(dossierLogExec.getPath() + "\\Step_" + Constantes.pathLogFileName);
			niv = 0;
			if (!logFileStep.exists()) 
				logFileStep.createNewFile();		
		}
		catch(Exception e)
		{
			System.err.println(e.getMessage());
		}
	}

	
	
	public void creerDossierExecution()
	{
		//On crée le dossier d'execution du cas de test
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    
		dossierLogExec = new File(dossierLogTest.getPath() + "\\" + strDate);
		if (!dossierLogExec.exists()) 
			dossierLogExec.mkdirs();
		
		//Deplacer le fichier de log dans le dossier d'execution
		logFile.renameTo(new File(dossierLogExec.getPath() + "\\" + logFile.getName()));
		logFile = new File(dossierLogExec.getPath() + "\\" + logFile.getName());

	}
	
	
	public void infoStart(String mess)
	{
		try
		{
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");//dd/MM/yyyy
			Date now = new Date();
			String strDate = sdfDate.format(now);
			LOGGER.info(mess);
			ecrire("info", sep + sep + sep + sep);
			ecrire("info", "\t" + mess + "\t\t\t " + strDate);
			ecrire("info", sep + sep + sep + sep);

			ecrire("step", sep + sep + sep + sep);
			ecrire("step", "\t" + mess + "\t\t\t " + strDate);
			ecrire("step", sep + sep + sep + sep);

		}catch(Exception e)
		{
			System.err.println(e.getCause().toString());
		}
	}


	public void info(String mess)
	{
		//LOGGER.info(mess);
		System.out.println(niv + tab + mess);
		ecrire("info", mess);
	}

	public void error(String mess)
	{
		//LOGGER.error(mess);
		System.out.println(mess);
		ecrire("error", mess);
	}

	public void warn(String mess)
	{
		//LOGGER.warn(mess);
		System.out.println(mess);
		ecrire("warn", mess);
	}

	public void debug(String mess)
	{
		//LOGGER.debug(mess);
		System.out.println(niv + tab + mess);
		ecrire("debug", mess);
	}

	public void step(String mess, String statut)
	{
		//LOGGER.warn(mess);
		niv++;
		mess = "[" + statut + "] " + mess;
		System.out.println(mess);
		ecrire("step", mess);
	}

	public void ecrire(String type, String log)
	{

		try{ 
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");//dd/MM/yyyy
			Date now = new Date();
			String strDate = sdfDate.format(now);
			FileWriter writerlog;
			switch (type) { 
			case "info":	
				writerlog = new FileWriter(logFileInfo,true);
				break;
			case "warn":
				writerlog = new FileWriter(logFileWarn,true);
				break;
			case "debug":
				writerlog = new FileWriter(logFileDebug,true);
				break;
			case "error":
				writerlog = new FileWriter(logFileError,true);
				break;
			case "step":	
				writerlog = new FileWriter(logFileStep,true);
				break;
			default:
				writerlog = new FileWriter(logFile,true);
			}
			//Log détallé
			writerlog.write("[" + strDate + "] " + log + "\n");
			writerlog.close();

			//Log global
			FileWriter writer= new FileWriter(logFile,true);
			writer.write("[" + strDate + "] - [" + type.toUpperCase() + "] " + log + "\n");
			writer.close();
		} 
		catch (IOException e1) { 
			System.err.println("IOException: " + e1); 
		} 

	}

	/**
	 * capturerEcran(WebDriver driver) : permet de capturer l'image de l'écran actif
	 * et de l'enregistrer au format .png dans le dossier du LOG
	 * @param driver
	 */
	public void capturerEcran(WebDriver driver)
	{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");//dd/MM/yyyy
			Date now = new Date();
			String strDate = sdfDate.format(now);
			FileUtils.copyFile(scrFile, new File(dossierLogExec.getPath() + "\\" + strDate + ".png"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void sauvegarderJDDComplet(){
		try
		{
			//On parcours les fichiers de données
			File[] fichiersDeDonneesASauvegarder = new File(Constantes.pathFichiersJDD).listFiles();

			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");//dd/MM/yyyy
			Date now = new Date();
			String strDate = sdfDate.format(now);

			//On crée le dossier d'execution du cas de test
			File dossierDeSauvegarde = new File(dossierLogExec.getPath() + "\\" + strDate);
			if (!dossierDeSauvegarde.exists())
				dossierDeSauvegarde.mkdir();

			//On sauvegarde les fichiers de données dans le dossier d'exécution
			for (int i=0; i <fichiersDeDonneesASauvegarder.length;i++) {
				Files.copy(fichiersDeDonneesASauvegarder[i], new File (dossierDeSauvegarde.getPath() + "\\" + fichiersDeDonneesASauvegarder[i].getName())); 
			}

			//On déplace le fichier de log, le pdf, les screenshots et le xml dans le dossier d'exécution
			File[] fichiersASauvegarder = dossierLogExec.listFiles();
			for (int i=0; i <fichiersASauvegarder.length;i++) {
				if(fichiersASauvegarder[i].isFile())
					fichiersASauvegarder[i].renameTo(new File(dossierDeSauvegarde.getPath() + "\\" + fichiersASauvegarder[i].getName()));
			}

		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

}
