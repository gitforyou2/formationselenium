package main.java.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;

import org.junit.Assert;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTWorksheet;

import com.opencsv.CSVReader;

import main.java.constantes.Constantes;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ReadExcel extends ApplicationCommonScripts{

	
	public String parsedText = null;
	
	/**
	 * Cette fonction permet de lire le fichier excel
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public XSSFSheet LireFichier (String file) throws Exception
    {
		String fileName=LOGGER.dossierLogExec.getPath() + "\\"+ file;	
		File fis = new File(fileName);		
		System.out.println(fis.getAbsolutePath().substring(fis.getAbsolutePath().lastIndexOf("\\")+1));
		FileInputStream inputStream = new FileInputStream(fis);
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        XSSFSheet sheet = workbook.getSheetAt(0);
        return sheet;
    }

	/**
	 * Cette fonction permet de convertir csv-> xlsx
	 * @param file
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	public void ConvertCSVtoXLSX (String fileCSV,String fileXLSX) throws Exception
    {
		//String csvFileAddress=file;
		String fileName=fileCSV;
		String xlsxFileAddress = LOGGER.dossierLogExec.getPath() + "\\"+ fileXLSX;
		fileName=LOGGER.dossierLogExec.getPath() + "\\"+ fileName;
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        String currentLine=null;
        int RowNum=0;
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        
        while ((currentLine = br.readLine()) != null) {
            String str[] = currentLine.split(";");
            XSSFRow currentRow=sheet.createRow(RowNum);
            for(int i=0;i<str.length;i++){
                currentRow.createCell(i).setCellValue(str[i]);
            }
            RowNum++;
        }
        
        FileOutputStream fileOutputStream =  new FileOutputStream(xlsxFileAddress);
        workbook.write(fileOutputStream);
        fileOutputStream.close();
        LOGGER.info("Convertion réussie");
    }

	/**
	 * Cette fonction permet de vérifier la colonne Habilitation I2A
	 * @param sheet
	 * @return
	 * @throws IOException
	 */	
	public Boolean verifColonneHabilitationI2A (XSSFSheet sheet) throws Exception
    {
		XSSFRow row = sheet.getRow(0);
		int col=row.getLastCellNum();
		Boolean verif=false;
		
		if(row.getCell(col).getStringCellValue().trim().equals("Habilitation I2A"))
    		{
				verif=true;
				LOGGER.step("La colonne Habilitation I2A est présente à la fin du fichier","OK");
    		}
		else{
			verif=false;
			LOGGER.error("La colonne Habilitation I2A n'est pas présente");}
		
		return verif;
    }
	
	
	
	/**
	 * Cette fonction permet de vérifier le fichier d'export de prêts
	 * @param sheet
	 * @return
	 * @throws IOException
	 */	
	public void verifColonneFilePret (XSSFSheet sheet) throws Exception
    {		
        XSSFRow row = sheet.getRow(0);
 
        for(int rowIndex=0; rowIndex < row.getLastCellNum(); rowIndex ++)
        {
        	//row = sheet.getRow(rowIndex);
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Téléphone"))
        		LOGGER.step("La colonne Téléphone est correcte","OK");

        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Type matériel"))
        		LOGGER.info("La colonne Type matériel est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Nature / Catégorie"))
        		LOGGER.info("La colonne Nature / Catégorie est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Agent"))
        		LOGGER.info("La colonne Agent est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Société"))
        		LOGGER.info("La colonne  Société est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Tournée"))
        		LOGGER.info("La colonne Tournée  est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Site de rattachement"))
        		LOGGER.info("La colonne Site de rattachement  est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Date de prêt"))
        		LOGGER.info("La colonne Date de prêt  est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Date de retour"))
        		LOGGER.info("La colonne Date de retour  est correcte");
        	
        	if(row.getCell(rowIndex).getStringCellValue().trim().equals("Statut"))
        		LOGGER.info("La colonne Statut  est correcte");        		  
        }
        LOGGER.info("Le tableau contient les bonnes colonnes"); 
    }
	
	
	/**
	 * Cette fonction permet de vérifier l'ajout des colonnes sur le fichier telephones disponibles
	 * @param sheet
	 * @return
	 * @throws IOException
	 */	
	public void verifColonnetelephones (XSSFSheet sheet) throws Exception
	{		
		XSSFRow row = sheet.getRow(0);
		
		for(int rowIndex=0; rowIndex < row.getLastCellNum(); rowIndex ++)
		{
			//row = sheet.getRow(rowIndex);
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("IMEI"))
				Assert.assertTrue(true);
			
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Numéro de téléphone"))
				Assert.assertTrue(true);
			
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Statut Téléphone"))
				Assert.assertTrue(true);
			
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("ID Titulaire"))
				Assert.assertTrue(true);
			
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Nom Titulaire"))
				Assert.assertTrue(true);
			
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Code Régate Site rattachement"))
				Assert.assertTrue(true);
			
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Site de rattachement"))
				Assert.assertTrue(true);
			
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Libellé Site rattachement"))
				Assert.assertTrue(true);
		
		}
		LOGGER.info("Le tableau contient les bonnes colonnes"); 
	}
	
	/**
	 * Cette fonction permet de vérifier l'ajout de la colonne Socle sur le fichier telephones affectés
	 * @param sheet
	 * @return
	 * @throws IOException
	 */	
	public void verifColonneSocle (XSSFSheet sheet) throws Exception
	{		
		XSSFRow row = sheet.getRow(0);
		int lastRow = sheet.getLastRowNum();
		
		for(int rowIndex=0; rowIndex < row.getLastCellNum(); rowIndex ++)
		{
			//row = sheet.getRow(rowIndex);
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Socle"))
				Assert.assertTrue(true);
		}
		
		for (int rowIndex = 1; rowIndex<lastRow; rowIndex++){
		    Row row1 = CellUtil.getRow(rowIndex, sheet);
		    Cell cell = CellUtil.getCell(row1, 3);
		    
		    if(cell.getStringCellValue().trim().contains("Airwatch") || cell.getStringCellValue().trim().contains("Google Apps Device"))
		    	Assert.assertTrue(true);
		}	
	}
	/**
	 * Cette fonction permet de vérifier le fichier d'agent 
	 * @param sheet
	 * @return
	 * @throws IOException
	 */	
	public void verifColonneAdresseLaposte (XSSFSheet sheet) throws Exception
	{		
		XSSFRow row = sheet.getRow(0);
		int cellIndex=0;
		for(int rowIndex=0; rowIndex < row.getLastCellNum(); rowIndex ++)
		{
			//row = sheet.getRow(rowIndex);
			if(row.getCell(rowIndex).getStringCellValue().trim().equals("Adresse facteo.fr"))
					cellIndex=rowIndex+1;			
		}
		
		if(row.getCell(cellIndex).getStringCellValue().trim().equals("Adresse laposte.fr"))				
			{
				LOGGER.step("La Adresse laposte.fr est correcte","OK");	
				Assert.assertTrue(true);
			}			

	}
	
	/**
	 * Cette fonction permet de vérifier le fichier d'agent 
	 * @param sheet
	 * @return
	 * @throws IOException
	 */	
	public void verifAjoutColonne(XSSFSheet sheet,String colInitial,String colNew) throws Exception
	{		
		XSSFRow row = sheet.getRow(0);
		int cellIndex=0;
		for(int rowIndex=0; rowIndex < row.getLastCellNum(); rowIndex ++)
		{
			//row = sheet.getRow(rowIndex);
			if(row.getCell(rowIndex).getStringCellValue().trim().equals(colInitial))
				cellIndex=rowIndex+1;			
		}
		
		if(row.getCell(cellIndex).getStringCellValue().trim().equals(colNew))				
		{
			LOGGER.step("La colonne "+colNew+" est bien présente après la colonne "+colInitial,"OK");	
			Assert.assertTrue(true);
		}			
		
	}

	/**
	 * Cette fonction permet de supprimer un fichier
	 * @param nomFichier
	 */
	public void supprimerFichier(String nomFichier){
		try{

			File file = new File(Constantes.pathTelechargement()+nomFichier);

			if(!file.delete()){
				LOGGER.info("Le fichier - "+file.getName() + " - est supprimé!");
			}else{
				LOGGER.error("Aucun fichier nommé\""  +file.getName() + "\" à supprimer");
			}

		}catch(Exception e){

			e.printStackTrace();

		}

	}

	/**
	 * Cette fonction permet de déplacer un fichier
	 * @param nomFichier
	 */
	public void deplacerFichier(String nomFichier){
		try{

			File file = new File(Constantes.pathTelechargement()+nomFichier);

			file.renameTo(new File(LOGGER.dossierLogExec.getPath() + "\\" + file.getName()));
			LOGGER.info("Le fichier - "+file.getName() + " - a bien été déplacé!");

		}catch(Exception e){
			LOGGER.error("Le fichier n'a pas été déplacé !.");
			e.printStackTrace();

		}

	}
	
	public void VerifierDonneesRemiseTelephone(Hashtable<String, String> dic){
		verifierIMEI(dic.get("imei"));
		verifierNumTel(dic.get("num_tel"));
		verifierTypeMateriel(dic.get("type_materiel"));
		verifierIdTitulaire(dic.get("id_titulaire"));
		verifierNomTitulaire(dic.get("nom"));
		verifierSiteRattachement(dic.get("site_rattachement"));
		verifierDateRemiseTelephone(dic.get("date_remise"));
		verifierSignature(dic.get("signature"));
		
	}
	
	
	public void verifierIMEI(String imei){
		
			verifierPresenceDonnee("IMEI");
	}

	public void verifierNumTel(String num_tel){
		verifierPresenceDonnee("Num tél");
	}

	public void verifierTypeMateriel(String type_materiel){
		verifierPresenceDonnee("Type matériel");
	}
	
	public void verifierIdTitulaire(String id_titulaire){
		verifierPresenceDonnee("ID Titulaire");
	}
	
	public void verifierNomTitulaire(String nom){
		verifierPresenceDonnee("Nom");
	}
	
	public void verifierSiteRattachement(String site_rattachement){
		verifierPresenceDonnee("Site de rattachement");
	}
	
	public void verifierDateRemiseTelephone(String date_remise){
		verifierPresenceDonnee("Date de remisedu téléphone");
	}
	
	public void verifierSignature(String signature){
		verifierPresenceDonnee("Signature");
	}

	/**
	 * Cette fonction permet de déplacer un fichier et de lui attribuer un nouveau nom
	 * @param nomFichier
	 * @param nouveauNomFichier
	 */
	public void deplacerFichier(String nomFichier, String nouveauNomFichier){
		try{

			File file = new File(Constantes.pathTelechargement()+nomFichier);

			file.renameTo(new File(LOGGER.dossierLogExec.getPath() + "\\" + nouveauNomFichier));
			LOGGER.info("Le fichier - "+file.getName() + " - a bien été déplacé!");


		}catch(Exception e){
			LOGGER.error("Le fichier n'a pas été déplacé !.");
			e.printStackTrace();

		}

	}



	public void VerifierDonneesParticulierMandat(DataList Liste) throws Exception{

		verifierPresenceDonnee(Liste.getData("Civilite"));
		verifierPresenceDonnee(Liste.getData("Nom"));
		verifierPresenceDonnee(Liste.getData("NomNaissance"));
		verifierPresenceDonnee(Liste.getData("Prenom"));
	}


	public void VerifierDonneesParticulierMandat(Hashtable<String, String> dic){

		verifierPresenceDonnee(dic.get("Civilite"));
		verifierPresenceDonnee(dic.get("Nom"));
		verifierPresenceDonnee(dic.get("NomNaissance"));
		verifierPresenceDonnee(dic.get("Prenom"));
	}



	public void VerifierAdresseMandat(DataList Liste) throws Exception{

		if (parsedText.contains(Liste.getData("NomVoie"))){
			LOGGER.info("L adresse est correct - Nom de la rue");
		} else {
			LOGGER.error("L adresse  est incorrect - Nom de la rue");
			Assert.assertTrue(false);
		}

		if (parsedText.contains(Liste.getData("CP"))){
			LOGGER.info("L adresse est correct - Code Posta");
		} else {
			LOGGER.error("L adresse  est incorrect - Code Posta");
			Assert.assertTrue(false);
		}
	}


	public void VerifierAdresseMandat(Hashtable<String, String> dic){

		if (parsedText.contains(dic.get("NomVoie"))){
			LOGGER.info("L adresse est correct - Nom de la rue");
		} else {
			LOGGER.error("L adresse  est incorrect - Nom de la rue");
			Assert.assertTrue(false);
		}

		if (parsedText.contains(dic.get("CP"))){
			LOGGER.info("L adresse est correct - Code Posta");
		} else {
			LOGGER.error("L adresse  est incorrect - Code Posta");
			Assert.assertTrue(false);
		}
	}


	public void VerifierDonneesEntreprise(DataList Liste) throws Exception{

		verifierTelEntreprise(Liste.getData("TelAvant"));
		verifierTelEntreprise(Liste.getData("TelApres"));
		verifierCourrielEntreprise(Liste.getData("Mail"));
		verifierSiretEntreprise(Liste.getData("Siret"));
		verifierRaisonSocialeEntreprise(Liste.getData("RaisonSociale"));
		verifierFormeJuridiqueEntreprise(Liste.getData("FormeJuridique"));
	}


	public void VerifierDonneesEntreprise(Hashtable<String, String> dic){

		verifierTelEntreprise(dic.get("TelAvant"));
		verifierTelEntreprise(dic.get("TelApres"));
		verifierCourrielEntreprise(dic.get("Mail"));
		verifierSiretEntreprise(dic.get("Siret"));
		verifierRaisonSocialeEntreprise(dic.get("RaisonSociale"));
		verifierFormeJuridiqueEntreprise(dic.get("FormeJuridique"));
	}

	public void VerifierDonneesParticulier(DataList Liste) throws Exception{

		verifierCivilite(Liste.getData("Civilite"));
		verifierNom(Liste.getData("Nom"));
		verifierNomNaissance(Liste.getData("NomNaissance"));
		verifierPrenom(Liste.getData("Prenom"));

		verifiertypeDePiecePID(Liste.getData("TypePDI"));
		verifierNumeroPID(Liste.getData("NumeroPDI"));
		verifierDateDelivrancePID(Liste.getData("DateDelivrancePDI"));
		verifierLieuDelivrancePID(Liste.getData("LieuDelivrancePDI"));
		verifierOrganismeDelivrancePID(Liste.getData("DelivrancePDI"));
		verifierCourriel(Liste.getData("Courriel"));
		verifierNumTelephone(Liste.getData("Telephone"));
	}


	public void VerifierDonneesParticulier(Hashtable<String, String> dic){

		verifierCivilite(dic.get("Civilite"));
		verifierNom(dic.get("Nom"));
		verifierNomNaissance(dic.get("NomNaissance"));
		verifierPrenom(dic.get("Prenom"));

		verifiertypeDePiecePID(dic.get("TypePDI"));
		verifierNumeroPID(dic.get("NumeroPDI"));
		verifierDateDelivrancePID(dic.get("DateDelivrancePDI"));
		verifierLieuDelivrancePID(dic.get("LieuDelivrancePDI"));
		verifierOrganismeDelivrancePID(dic.get("DelivrancePDI"));
		verifierCourriel(dic.get("Courriel"));
		verifierNumTelephone(dic.get("Telephone"));
	}


	public void VerifierAdressePDF(DataList Liste) throws Exception{

		verifierAdresseCP(Liste.getData("CP"));
		verifierAdresseVoie(Liste.getData("NomVoie"));
	}

	public void VerifierAdressePDF(Hashtable<String, String> dic){

		verifierAdresseCP(dic.get("CP"));
		verifierAdresseVoie(dic.get("NomVoie"));
	}

	public void VerifierAdresseLieuDitPDF(Hashtable<String, String> dic){

		String lieuDit = dic.get("LieuDit").toUpperCase();

		if (parsedText.contains("BP ou LIEU DIT "+lieuDit)){
			LOGGER.info("Le Lieu dit est correct");
		} else {
			LOGGER.error("Le Lieu dit est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void VerifierAdresseBureeauArriveePDF(DataList Liste) throws Exception{

		if (parsedText.contains(Liste.getData("Bureau"))){
			LOGGER.info("L adresse de bureau d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de bureau d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}
/*
	public void VerifierStatut(Hashtable<String, String> dic){

		String statutAttendu = "";

		if(dic.get("Statut").equals(Constantes.Supprime))
			statutAttendu = Constantes.ReexpPDFContratSupprimePrefixe + dic.get("DateMAJ") + Constantes.ReexpPDFContratSupprimeSuffixe;
		else if(dic.get("Statut").equals(Constantes.Modifie))
			statutAttendu = Constantes.ReexpPDFContratModifiePrefixe + dic.get("DateMAJ") + Constantes.ReexpPDFContratModifieSuffixe;
		else if(dic.get("Statut").equals(Constantes.Nouveau))
			statutAttendu = Constantes.ReexpPDFContratNouveauPrefixe + dic.get("DateMAJ") + Constantes.ReexpPDFContratNouveauSuffixe;

		if (parsedText.contains(statutAttendu)){
			LOGGER.info("Le Statut: '" + statutAttendu + "' a été trouvé ");
		} 
		else {
			LOGGER.error("le Statut: '" + statutAttendu + "' n'a pas été trouvé ");
			Assert.assertTrue(false);
		}
	}*/




	public void VerifierAdresseBureauArriveePDF(Hashtable<String, String> dic){

		if (parsedText.contains(dic.get("Bureau"))){
			LOGGER.info("L adresse de bureau d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de bureau d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void VerifierAdressePaysArriveePDF(DataList Liste) throws Exception{

		if (parsedText.contains(Liste.getData("Pays"))){
			LOGGER.info("L adresse de Pays d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de Pays d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void VerifierAdressePaysArriveePDF(Hashtable<String, String> dic){

		if (parsedText.contains(dic.get("Pays"))){
			LOGGER.info("L adresse de Pays d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de Pays d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierCivilite(String civilite){

		if (parsedText.contains(civilite+" Nom")){
			LOGGER.info("La civilite est correct");
		} else {
			LOGGER.error("La civilite est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierNom(String nom){
		nom=nom.toUpperCase();
		if (parsedText.contains("Nom : "+nom)){
			LOGGER.info("Le nom est correct");
		} else {
			LOGGER.error("Le nom est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierNomNaissance(String nomNaissance){
		nomNaissance=nomNaissance.toUpperCase();
		if (parsedText.contains(nomNaissance)||parsedText.contains(nomNaissance)){
			LOGGER.info("Le nom de naissance est correct");
		} else {
			LOGGER.error("Le nom de naissance est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierPrenom(String prenom){
		prenom=prenom.toUpperCase();
		if (parsedText.contains("Prénom : "+prenom)){
			LOGGER.info("Le prenom est correct");
		} else {
			LOGGER.error("Le prenom est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifiertypeDePiecePID(String piece){

		if (parsedText.contains("pièce : "+piece+" n°")){
			LOGGER.info("Le type de la piece d identite est correct");
		} else {
			LOGGER.error("Le type de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierNumeroPID(String numero){

		if (parsedText.contains("n° "+numero)){
			LOGGER.info("Le numero de la piece d identite est correct");
		} else {
			LOGGER.error("Le numero de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierDateDelivrancePID(String date){

		if (parsedText.contains("le : "+date)){
			LOGGER.info("La date de delivrance de la piece d identite est correcte");
		} else {
			LOGGER.error("La date de delivrance de la piece d identite est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierLieuDelivrancePID(String lieu){
		lieu=lieu.toUpperCase();
		if (parsedText.contains("à : "+lieu)){
			LOGGER.info("Le lieu de delivrance de la piece d identite est correct");
		} else {
			LOGGER.error("Le lieu de delivrance de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierOrganismeDelivrancePID(String organisme){

		if (parsedText.contains("Par : "+organisme)){
			LOGGER.info("L organisme de delivrance de la piece d identite est correct");
		} else {
			LOGGER.error("L organisme de delivrance de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierCourriel(String mail){

		if (parsedText.contains(mail)){
			LOGGER.info("L adresse email est correcte");
		} else {
			LOGGER.error("L adresse email est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierNumTelephone(String num){
		if (!num.equals(""))
			num=num.substring(0, 2)+'.'+num.substring(2, 4)+'.'+num.substring(4, 6)+'.'+num.substring(6, 8)+'.'+num.substring(8, 10);
		if (parsedText.contains("phone : "+num)){
			LOGGER.info("Le numero de telephone est correct");
		} else {
			LOGGER.error("Le numero de telephone est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierAdresseCP(String cp){

		if (parsedText.contains("LOCALITE "+cp)){
			LOGGER.info("Le CP est correct");
		} else {
			LOGGER.error("Le CP est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierAdresseVoie(String voie){

		if (parsedText.contains(voie)){
			LOGGER.info("Le nom de la voie est correct");
		} else {
			LOGGER.error("Le nom de la voie est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierTelEntreprise(String num){
		if (!num.equals(""))
			num=num.substring(0, 2)+'.'+num.substring(2, 4)+'.'+num.substring(4, 6)+'.'+num.substring(6, 8)+'.'+num.substring(8, 10);
		if (parsedText.contains("adresse : "+num)){
			LOGGER.info("Le numero de telephone est correct");
		} else {
			LOGGER.error("Le numero de telephone est incorrect");
			Assert.assertTrue(false);
		}
	}
	public void verifierCourrielEntreprise(String mail){

		if (parsedText.contains("E-mail * : "+mail)){
			LOGGER.info("L adresse email est correcte");
		} else {
			LOGGER.error("L adresse email est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierSiretEntreprise(String code){

		if (parsedText.contains(code)){
			LOGGER.info("Le Code SIRET est correct");
		} else {
			LOGGER.error("Le Code SIRET est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierRaisonSocialeEntreprise(String raison){

		if (parsedText.contains(raison)){
			LOGGER.info("Le Raison Sociale est correct");
		} else {
			LOGGER.error("Le Raison Sociale est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierFormeJuridiqueEntreprise(String forme){

		if (parsedText.contains("Forme juridique : "+forme)){
			LOGGER.info("Le Forme Juridique est correct");
		} else {
			LOGGER.error("Le Forme Juridique est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierPresenceDonnee(String donnee){
		//donnee=donnee.toUpperCase();
		if (parsedText.contains(donnee)){
			LOGGER.info("Le donnee "+donnee+" est correct");
		} else {
			LOGGER.error("Le donnee "+donnee+" est incorrect");
			Assert.assertTrue(false);
		}
	}
	
	/**
	 * Cette fonction permet de vérifier le fichier d'agent 
	 * @param sheet
	 * @return
	 * @throws IOException
	 */	
	public void verifInvisibiliteAdresse (XSSFSheet sheet) throws Exception
	{		
		XSSFRow row = sheet.getRow(0);
		
		
		for(int rowIndex=0; rowIndex < row.getLastCellNum(); rowIndex ++)
			
		{
			
			
			if(!(row.getCell(rowIndex).getStringCellValue().trim().equals("Mois de déploiement")))
        		LOGGER.step("La colonne Mois de déploiement est invisible","OK");

			if(!(row.getCell(rowIndex).getStringCellValue().trim().equals("Adresse facteo.fr")))
        		LOGGER.step("La colonne Adresse facteo.fr est invisible","OK");
			
			if(!(row.getCell(rowIndex).getStringCellValue().trim().equals("Adresse mfacteur.fr")))
        		LOGGER.step("La colonne Adresse mfacteur.fr est invisible","OK");
			
			if(!(row.getCell(rowIndex).getStringCellValue().trim().equals("Compte Store Facteo")))
        		LOGGER.step("La colonne Compte Store Facteo est invisible","OK");
			
			
	}
	
	
	}
	
	public Boolean verifAbsenceColonne(XSSFSheet sheet,String colInitial) throws Exception
	{		

		Boolean verif=false;

		
		XSSFRow row = sheet.getRow(0);
		
		int index = 0;
//		int top = sheet.getFirstRowNum();
//     int bottom = sheet.getLastRowNum();
		for (int rowIndex = 0 ; rowIndex < row.getLastCellNum();rowIndex ++) {
			
				if (!(row.getCell(rowIndex).getStringCellValue().trim().equals(colInitial))) {
					verif = true;
					LOGGER.step("La colonne " + colInitial + " est absente sur le fichier", "OK");
				} else {
					verif = false;
					LOGGER.error("La colonne " + colInitial + " est présente");
					Assert.assertTrue(false);
					
				}
		
		}
		return verif;

	}
		
	public Boolean verifAbsenceOglet(XSSFSheet sheet, String colInitial) throws Exception {

		Boolean verif = false;

		String Onglet = sheet.getSheetName();

		if (Onglet.equals(colInitial))

		{
			verif = true;
			LOGGER.step("L'onglet " + colInitial + " est prèsente sur le fichier", "OK");

		} else {
			verif = false;
			LOGGER.error("La colonne " + colInitial + " est absente");

		}

		return verif;

	}
	

}
