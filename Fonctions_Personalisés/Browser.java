package main.java.utils;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;




@SuppressWarnings("rawtypes")
public class Browser extends ApplicationCommonScripts{
	
	//====================================================================================================
	//DECLARATIONS
	//====================================================================================================
	private String browserName;
	private String defaultBrowser;
	private String browserChrome;
	private String browserFirefox;
	private String browserIE;
	private String domaine;
	
	//====================================================================================================
	//METHODES
	//====================================================================================================
	//Constructeur
	public Browser() throws Exception{
		getBrowserProperties();
		
		//Attribution d'une valeur par défaut si le navigateur n'est pas renseigné
		if (StringUtils.isBlank(System.getProperty(ap.browserPropertyName))){
			LOGGER.debug("La paramètre système browser n'est pas renseigné.");
			System.setProperty(ap.browserPropertyName, defaultBrowser);
			if (System.getProperty(ap.browserPropertyName).equalsIgnoreCase(defaultBrowser)){
				LOGGER.debug("Attribution d'une valeur par défaut: browser=" + System.getProperty(ap.browserPropertyName));
			} else {
				String msg = "Aucune valeur par défaut n'a été défini pour la propriété browser.";
				LOGGER.error(msg);
				throw new Exception(msg);
			}
		}
		browserName = System.getProperty(ap.browserPropertyName);
	}
	
	public void getBrowserProperties() throws Exception{
		//DataList listeParametre = new DataList(ap.propertiesFile);
		defaultBrowser = listeParametre.getData("browser.default");
		browserChrome = listeParametre.getData("browser.chrome");
		browserFirefox = listeParametre.getData("browser.firefox");
		browserIE =listeParametre.getData("browser.internetExplorer");
	}
	
	public FirefoxOptions firefoxDriverOption() throws Exception {
		Proxy proxy = new Proxy();
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");

		//cap.setCapability(CapabilityType.PROXY, proxy);
		DesiredCapabilities cap = new DesiredCapabilities();
		 //String proxyAutoconfigUrl = "file://///SF1Coeur/SOG-LAPOSTE-BSCC-CDS-TESTS/proxy.pac";
		String proxyAutoconfigUrl =  listeParametre.getData("Proxy.Url");
		cap.setBrowserName("firefox");
		cap.setCapability("marionette", true);
//		cap.setVersion("60.4.0esr");
//		cap.setPlatform(Platform.WINDOWS);
		cap.setCapability("webdriver.firefox.logfile", false);
		cap.setCapability("acceptInsecureCerts", true);
		//cap.setCapability("nativeEvents", true);
		
		// Set Preferences for FirefoxOption
		FirefoxOptions options = new FirefoxOptions(cap);
		//options.setLegacy(true);
		options.addPreference("browser.tabs.remote.autostart", false);
		options.addPreference("browser.tabs.remote.autostart.1", false);
		options.addPreference("browser.tabs.remote.autostart.2", false);


		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("network.proxy.type", 1);
		profile.setPreference("network.proxy.autoconfig_url", proxyAutoconfigUrl);
		profile.setPreference("network.proxy.socks","10.70.98.120");
		profile.setPreference("network.proxy.socks_port", 8080);
		profile.setPreference("network.proxy.http","10.70.98.120");
		profile.setPreference("network.proxy.http_port", 8080);
		profile.setPreference("network.proxy.ssl","10.70.98.120");
		profile.setPreference("network.proxy.ssl_port", 8080);
		profile.setPreference("network.proxy.share_proxy_settings",Boolean.TRUE);
		options.setProfile(profile);
		return options;
//		profile.setPreference("network.proxy.type", 1);
//		profile.setPreference("network.proxy.autoconfig_url", proxyAutoconfigUrl);
//
//		
//		profile.setPreference("network.proxy.socks","10.70.98.120");
//		profile.setPreference("network.proxy.socks_port", 8080);
//		profile.setPreference("network.proxy.http","10.70.98.120");
//		profile.setPreference("network.proxy.http_port", 8080);
//		profile.setPreference("network.proxy.ssl","10.70.98.120");
//		profile.setPreference("network.proxy.ssl_port", 8080);
//		profile.setPreference("network.proxy.share_proxy_settings",Boolean.TRUE);
//		options.setProfile(profile);
//		return options;
	}
	
	
	
	public WebDriver getDriver() throws Exception{
		domaine = listeParametre.getData("Domaine.Name");
		String domain;
		domain = System.getenv("USERDOMAIN");
		if (domain.equals(domaine)){
		if(browserName.equalsIgnoreCase(browserFirefox)){
			driver = new FirefoxDriver(firefoxDriverOption());
		//driver = new FirefoxDriver();
			LOGGER.info("Ouverture de Firefox.");
		} else if(browserName.equalsIgnoreCase(browserChrome)){
			System.setProperty("webdriver.chrome.driver", "//sf1coeur/SOG-LAPOSTE-BSCC-CDS-TESTS/chromedriver_v2.45.exe");
			String myProxy = "10.70.98.120:8080";  //example: proxy host=10.70.98.120 port=8080
			DesiredCapabilities capabilities = new DesiredCapabilities();
			Proxy proxy = new Proxy();
			proxy.setHttpProxy(myProxy);
			proxy.setSslProxy(myProxy);
			String noProxy= "localhost, 127.0.0.1, .google.com";
			proxy.setNoProxy(noProxy);
			capabilities.setCapability(CapabilityType.PROXY, proxy);
			//capabilities.setCapability(CapabilityType.HAS_NATIVE_EVENTS, false);
			driver = new ChromeDriver();
			WebDriverWait wait = new WebDriverWait(driver, 10);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
			//driver = new ChromeDriver();
			LOGGER.info("Ouverture de Chrome.");
		} else if(browserName.equalsIgnoreCase(browserIE)){
			driver = new InternetExplorerDriver();
			LOGGER.info("Ouverture d'Internet Explorer.");
		} else {
			String msg = "Le paramètre système browser " + browserName + " est invalide - "
			+ "Valeurs possibles: " 
			+ LF + browserFirefox
			+ LF + browserChrome
			+ LF + browserIE;
			LOGGER.error(msg);
			throw new Exception(msg);
		}
		}
		return driver;

	}
}