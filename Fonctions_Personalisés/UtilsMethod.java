package main.java.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.PaneInformation;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.AutoFilter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellRange;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.Header;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SheetConditionalFormatting;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import main.java.constantes.Constantes;

public class UtilsMethod {

	private XSSFWorkbook workbook = new XSSFWorkbook();
	private static List<List<HSSFCell>> cellGrid; 
//	private XSSFWorkbook workbook = new XSSFWorkbook();
	// private static File file;
	// private static CsvFileWriter csvFileWriter;
	/*
	 * Take a Map of integer and List of string and Create a Xlsx file on given path
	 * with number of row equal to size of nameList And number of Cell equal to
	 * keyset size
	 */
	public void initializeExcelFile(Map<Integer, List<String>> hashmap, File filepath) throws IOException {

		FileOutputStream out = new FileOutputStream(filepath);
		Set<Integer> keyset = hashmap.keySet();
		XSSFSheet sheet = workbook.createSheet();
		// XSSFRow row = null;
		List<String> nameList = hashmap.get(keyset.toArray()[0]);
		String idrh = "IDRH";
		String email = "EMAIL";
		int rownum = 0;
		Cell cell;
		Row row;
		// Ecrire entete fichier
		HSSFWorkbook wb = new HSSFWorkbook();
		// XSSFRow row1 = sheet.createRow(0);
		row = sheet.createRow(rownum);
		// XSSFCell cell = row1.createCell(0);
		// cell.setCellValue(idrh);
		// IDRH
		cell = row.createCell(0);
		cell.setCellValue(idrh);
		// EMAIL
		cell = row.createCell(1);
		cell.setCellValue(email);

		for (int j = 1; j < nameList.size() + 1; j++) {
			row = sheet.createRow(j);
			if (null != row) {
				for (int i = 0; i < keyset.size(); i++) {
					row.createCell(i);
				}
			}
		}

		workbook.write(out);
		// out.close();
	}

	/*
	 * Use initializeExcelFile(hashmap,path) to initialize a Xlsx file in given path
	 * After that, write the content of hashmap into Xlsx file
	 */
	public void writeToExcelfile(Map<Integer, List<String>> hashmap, File filepath) throws IOException {

		Set<Integer> keyset = hashmap.keySet();
		InputStream inp = new FileInputStream(filepath);
		FileOutputStream out = new FileOutputStream(filepath);
		int rownum = 1;
		int cellnum = 0;

		initializeExcelFile(hashmap, filepath);

		workbook = new XSSFWorkbook(inp);

		XSSFSheet sheet = workbook.getSheetAt(0);
		for (Integer key : keyset) {
			List<String> nameList = hashmap.get(key);
			for (String s : nameList) {

				XSSFRow row = sheet.getRow(rownum++);
				Cell cell = row.getCell(cellnum);
				if (null != cell) {
					cell.setCellValue(s);
				}
			}
			cellnum++;
			rownum = 1;
		}
		workbook.write(out);
		out.close();
		inp.close();
		System.out.println("Writesheet.xlsx written successfully");
	}



	public void writeToCSVfile(Map<Integer, List<String>> hashmap, String path) throws IOException {

		Set<Integer> keyset = hashmap.keySet();
		InputStream inp = new FileInputStream(new File(path));
		FileOutputStream out = new FileOutputStream(new File(path));
		int rownum = 1;
		int cellnum = 0;

		initializeCSVlFile(hashmap, path);

		workbook = new XSSFWorkbook(inp);

		XSSFSheet sheet = workbook.getSheetAt(0);
		for (Integer key : keyset) {
			List<String> nameList = hashmap.get(key);
			for (String s : nameList) {

				XSSFRow row = sheet.getRow(rownum++);
				Cell cell = row.getCell(cellnum);
				if (null != cell) {
					cell.setCellValue(s);
				}
			}
			cellnum++;
			rownum = 1;
		}
		workbook.write(out);
		out.close();
		inp.close();
		System.out.println("Writesheet.xlsx written successfully");
	}

	public void initializeCSVlFile(Map<Integer, List<String>> hashmap, String path) throws IOException {

		// first create file object for file placed at location
		// specified by filepath
		final String COMMA_DELIMITER = ";";
		final String NEW_LINE_SEPARATOR = System.getProperty("line.separator");
		File file = new File(path);
		Set<Integer> keyset = hashmap.keySet();
		InputStream inp = new FileInputStream(file);
        FileOutputStream out = new FileOutputStream(file);
		
       

		int rownum = 1;
		int cellnum = 0;
		try {
			
			// create FileWriter object with file as parameter

            // Value
			final String FILE_HEADER = "IDRH;EMAIL";
			FileWriter fileWriter = null;
			fileWriter = new FileWriter(path);
			// Write the CSV file header
			fileWriter.append(FILE_HEADER.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);

            String[] keysArray = new String[hashmap.keySet().size()];
            String[] valuesArray = new String[hashmap.values().size()];
            Sheet s1= workbook.getSheetAt(0); 
            CSVWriter writer = new CSVWriter(fileWriter);
            for (Integer key : keyset){
            	List<String> nameList =  hashmap.get(key);
            	for (String s : nameList) {
            		
            		Row row1 = s1.getRow(rownum++);
            		Cell cell = row1.getCell(cellnum);
            		if (null != cell) {
    					cell.setCellValue(s);
    				}
				}
				cellnum++;
				rownum = 1;
            }

			// closing writer connection
            writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	 

//     public   void convert(File inputFile, File outputFile) {
//
//     try {
//         FileOutputStream fos = new FileOutputStream(outputFile);
//         // Get the workbook object for XLSX file
//         XSSFWorkbook wBook = new XSSFWorkbook(
//                 new FileInputStream(inputFile));
//         // Get first sheet from the workbook
//         XSSFSheet sheet = wBook.getSheetAt(0);
//         // For storing data into CSV files
//         StringBuffer data = new StringBuffer();
//         Row row;
//         Cell cell;
//         // Iterate through each rows from first sheet
//         Iterator<Row> rowIterator = sheet.iterator();
//
//         while (rowIterator.hasNext()) {
//             row = rowIterator.next();
//
//             // For each row, iterate through each columns
//             Iterator<Cell> cellIterator = row.cellIterator();
//             while (cellIterator.hasNext()) {
//
//                 cell = cellIterator.next();
//
//                 switch (cell.getCellType()) {
//                 case Cell.CELL_TYPE_BOOLEAN:
//                     data.append(cell.getBooleanCellValue());
//
//                     break;
//                 case Cell.CELL_TYPE_NUMERIC:
//                     data.append(cell.getNumericCellValue());
//
//                     break;
//                 case Cell.CELL_TYPE_STRING:
//                     data.append(cell.getStringCellValue());
//                     break;
//
//                 case Cell.CELL_TYPE_BLANK:
//                     data.append("" + ",");
//                     break;
//                 default:
//                     data.append(cell);
//
//                 }
//             }
//         }
//
//         fos.write(data.toString().getBytes());
//         fos.close();
//
//     } catch (Exception ioe) {
//         ioe.printStackTrace();
//     }
// }

	
	   public void xls(File inputFile) {
	        // Get the workbook object for XLS file
	        int count = 0;
	        try {

	        	XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(
	                    inputFile));
	            for (int l = workbook.getNumberOfSheets() - 1; l >= 0; l--) {
	                File outputFile = new File(System.getProperty("user.dir")
	                        + "/output/"+inputFile.getName()+"-"+workbook.getSheetName(count) +".csv");

	                // For storing data into CSV files
	                StringBuffer data = new StringBuffer();
	                FileOutputStream fos = new FileOutputStream(outputFile);
	                XSSFSheet sheet = workbook.getSheetAt(count);
	                Cell cell;
	                Row row;

	                // Iterate through each rows from first sheet
	                Iterator<Row> rowIterator = sheet.iterator();
	                while (rowIterator.hasNext()) {
	                    row = rowIterator.next();
	                    // For each row, iterate through each columns
	                    Iterator<Cell> cellIterator = row.cellIterator();
	                    int columnNumber = 1;
	                    int rownum = 1;
	                    while (cellIterator.hasNext()) {
	                        cell = cellIterator.next();
	                        if (columnNumber > 1)
	                        {
	                            data.append(",");
	                        	
	                        }

	                        switch (cell.getCellType()) {
	                        case Cell.CELL_TYPE_BOOLEAN:
	                            data.append(cell.getBooleanCellValue());
	                            data.append('\r');
	                            break;

	                        case Cell.CELL_TYPE_NUMERIC:
	                            data.append(cell.getNumericCellValue() );
	                            data.append('\f');
	                            break;

	                        case Cell.CELL_TYPE_STRING:
	                            data.append(cell.getStringCellValue());
//	                            data.append('\f');
	                           
	                            break;

	                        case Cell.CELL_TYPE_BLANK:
	                            data.append("");
	                            data.append('\r');
	                            break;

	                        default:
	                            data.append(cell);
	                        }
	                         ++columnNumber;
	                    }
	                    data.append('\n');
	                }

	                fos.write(data.toString().getBytes());
	                fos.close();
	                count++;
	            }
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

		/*
		 * Cette fonction permet de convertir un fichier excel to csv
		 */
	   public static void convertExcelToCsv(String pathXLS, String pathCSV) throws IOException {
			try {
				
				cellGrid = new ArrayList<List<HSSFCell>>();
				FileInputStream myInput = new FileInputStream(pathXLS);
				POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);
				HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);
				HSSFSheet mySheet = myWorkBook.getSheetAt(0);
				Iterator<?> rowIter = mySheet.rowIterator();
//				File file = new File(pathXLS);
//				File file1 = new File(pathCSV);
				while (rowIter.hasNext()) {
					HSSFRow myRow = (HSSFRow) rowIter.next();
					Iterator<?> cellIter = myRow.cellIterator();
					List<HSSFCell> cellRowList = new ArrayList<HSSFCell>();
					while (cellIter.hasNext()) {
						HSSFCell myCell = (HSSFCell) cellIter.next();
						cellRowList.add(myCell);
					}
					cellGrid.add(cellRowList);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			File file = new File(pathCSV);
			PrintStream stream = new PrintStream(file, "utf-8");
			for (int i = 0; i < cellGrid.size(); i++) {
				List<HSSFCell> cellRowList = cellGrid.get(i);
				for (int j = 0; j < cellRowList.size(); j++) {

					if (i == 0) {
						HSSFCell myCell = (HSSFCell) cellRowList.get(j);
						String stringCellValue = myCell.toString();
						stream.print(stringCellValue + ";");
					} else {
						HSSFCell myCell = (HSSFCell) cellRowList.get(j);
						String stringCellValue = myCell.toString();

//						if (stringCellValue.equals(Constantes.cp + ".0"))
//							stringCellValue = Constantes.cp;
						
						if (stringCellValue.contains(".")) 		
						stringCellValue= stringCellValue.replace(".0", "");
						
						
						stream.print(stringCellValue + ";");
						
						if (j == 3) {
							stream.print(";");
							stream.print(";");
						}
						if (j == 4) {
							stream.print(";");
						}

					
					if (j == 6) {
						stream.print(";");
						stream.print(";");
					}

				}

				}
				stream.println("");
			}
		}  
	   
	 

	}


