package main.java.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import main.java.constantes.Constantes;
import main.java.pages.PageI2A;
import main.java.pages.Google.PageCompteGoogle;
import main.java.pages.PageGestionHabilitations.PageGestionHabilitations;
import main.java.pages.Pradeo.PagePradeo;
import main.java.pages.StoreAppaloosa.PageCompteStore;
import main.java.pages.TEM.PageTEM;

public class ConfigConnexion extends SetupTeardown{
	
	public  static String 	prefix = "src/main/resources/configuration/";
	public  static PageI2A pI2A;	
	public  static PageGestionHabilitations pGestionHabilitations;
	public  static PageCompteGoogle pCompteGoogle;
	public  static PageCompteGoogle pCompteStore;
	public static Process connexionPutty;
	public static String domaine;
	
	
	/**
	 * Méthode Effectuant toute la partie entre le Wac et le choix de l'application sur lequel on veut se connecter
	 * @return
	 * @throws SeleniumException
	 */
	public static WebDriver initialisationDriver() throws Exception{

		// Fonctionnement depuis Eclipse  user.dir => racine du projet
				
		if(Files.exists(Paths.get(System.getProperty("user.dir") + "\\geckodriver.exe")))
		{
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir") + "\\geckodriver.exe");
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
		}
		else
		{
			// Fonctionnement depuis Maven  user.dir = racine du projet\target
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir") + "\\..\\geckodriver.exe");
		}

		String browserPath = null;
		domaine = listeParametre.getData("Domaine.Name");
		String domain;
		domain = System.getenv("USERDOMAIN");
		if (domain.equals(domaine)){
			browserPath = "C:\\Program Files\\Mozilla Firefox\\firefox.exe";
		}else {
//			browserPath = "C:\\Temp\\Firefox\\firefox.exe"	;
			browserPath = "C:\\Users\\cdstest1\\Desktop\\Mozilla Firefox\\firefox.exe";
		}

		DesiredCapabilities cap = new DesiredCapabilities();

		cap.setBrowserName("firefox");
		cap.setCapability("marionette", true);
		cap.setCapability("acceptInsecureCerts", true);

		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,
				"/dev/null");
		
		cap.setCapability("webdriver.firefox.logfile", false);
		cap.setCapability("acceptInsecureCerts", true);
		
		FirefoxOptions options = new FirefoxOptions(cap);
		FirefoxBinary bin = configurerBinary(browserPath);

		options.addPreference("browser.tabs.remote.autostart", false);
		options.addPreference("browser.tabs.remote.autostart.1", false);
		options.addPreference("browser.tabs.remote.autostart.2", false);

		options.setProfile(configurerProfil());
		options.setBinary(bin);
		driver = new FirefoxDriver(options);

		return driver;
	}
	/**
	 * preciser le chemin du bin firefox a utiliser
	 * 
	 * @param repertoireExecutable
	 * @return
	 * @author gdx126
	 */
	public static FirefoxBinary configurerBinary(String repertoireExecutable) {
		File pathToFirefoxBinary = new File(repertoireExecutable);
		FirefoxBinary bin = new FirefoxBinary(pathToFirefoxBinary);
		return bin;
	}
	
	public static FirefoxProfile configurerProfil() throws Exception {

		FirefoxProfile profile = new FirefoxProfile();

		Proxy proxy = new Proxy();

		String domain;
		domain = System.getenv("USERDOMAIN");
		
		if (domain.equals(domaine)){
			//String proxyAutoconfigUrl = "file:///D:/Appli/proxy.pac";
			String proxyAutoconfigUrl = listeParametre.getData("Proxy.Url");
			proxy.setProxyType(Proxy.ProxyType.PAC);
			proxy.setProxyAutoconfigUrl(proxyAutoconfigUrl);

			profile.setPreference("network.proxy.type", 2);
			profile.setPreference("network.proxy.autoconfig_url", proxyAutoconfigUrl);

			// Set the http proxy to 10.70.98.120:8080
			System.setProperty("http.proxyHost", "10.70.98.120");
			System.setProperty("http.proxyPort", "8080");
		} else {
			profile.setPreference("network.proxy.type", 0);
		}

		profile.setPreference("network.auth.use-sspi", Boolean.FALSE);
		profile.setPreference("capability.policy.strict.Window.alert", "noAccess");
		profile.setPreference("network.negotiate-auth.trusted-uris", "http://www.wac.courrier.intra.laposte.fr/,http://idp.si-tri.com/,https://gestri.recdtc.dip.courrier.intra.laposte.fr/gestri/,https://selfservice.acc-authentification.courrier.intra.laposte.fr/account/protected/habilitations.xhtml");
		profile.setPreference("network.automatic-ntlm-auth.trusted-uris",
				"http://www.wac.courrier.intra.laposte.fr/,http://idp.si-tri.com/,gestri.assemblage.net3-courrier.extra.laposte.fr,https://gestri.recdtc.dip.courrier.intra.laposte.fr/gestri/,https://selfservice.acc-authentification.courrier.intra.laposte.fr/account/protected/habilitations.xhtml,http://wttraceo.net-courrier.extra.laposte.fr/");

		profile.setPreference("extensions.enabledAddons", "fxdriver@googlecode.com:2.25.0");
		profile.setPreference("jsprintsetup.disabled", true);
		profile.setPreference("jsprintsetup.firstRun", false);
		profile.setPreference("extensions.installCache", "");


		try {
			profile.setAcceptUntrustedCertificates(true);
			profile.setAssumeUntrustedCertificateIssuer(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		profile.setPreference("xpinstall.signatures.required", false);
		return profile;
	}
	
	/**
	 * Cette fonction permet de s identifier sur I2A
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageGestionHabilitations seConnecter(WebDriver driver) throws IOException, Exception{
		
		pI2A=PageFactory.initElements(driver, PageI2A.class);
		pI2A.seConnecterHabileo();
		pGestionHabilitations=PageFactory.initElements(driver, PageGestionHabilitations.class);
		return pGestionHabilitations;
	}
	
	/**
	 * Se connecter a Gmail
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageCompteGoogle connexionGoogle(WebDriver driver) throws IOException, Exception{
		connexionPutty = new ProcessBuilder(ap.PathAutoIt+ap.AuthentificationRequise,Constantes.user, Constantes.mdp).start();	
		System.setProperty(ap.urlPropertyNameAdm,new Environment("google").getURL());	
		driver.get(System.getProperty(ap.urlPropertyNameAdm));
		return  PageFactory.initElements(driver, PageCompteGoogle.class);
	}
	
	/**
	 * Se connecter a appaloosa (store)
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageCompteStore connexionStore(WebDriver driver) throws IOException, Exception{
		connexionPutty = new ProcessBuilder(ap.PathAutoIt+ap.AuthentificationRequise,Constantes.user, Constantes.mdp).start();	
		System.setProperty(ap.urlPropertyNameAdm,new Environment("store").getURL());	
		driver.get(System.getProperty(ap.urlPropertyNameAdm));
		return  PageFactory.initElements(driver, PageCompteStore.class);
	}
	
	/**
	 * Se connecter au TEM
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageTEM connexionTem(WebDriver driver) throws IOException, Exception{
		 
		System.setProperty(ap.urlPropertyNameTem,new Environment("tem").getURL());	
		driver.get(System.getProperty(ap.urlPropertyNameTem));
		return  PageFactory.initElements(driver, PageTEM.class);
	}
	
	/**
	 * Se connecter a Pradeo
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PagePradeo connexionPradeo(WebDriver driver) throws IOException, Exception{
		
		System.setProperty(ap.urlPropertyNamePradeo,new Environment("pradeo").getURL());	
		driver.get(System.getProperty(ap.urlPropertyNamePradeo));
		return  PageFactory.initElements(driver, PagePradeo.class);
	}
	
	/**
	 * Se connecter a Habileo en tant que Administrateur
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageGestionHabilitations connexionAdm(WebDriver driver) throws IOException, Exception{
		
			System.setProperty(ap.urlPropertyNameAdm,new Environment("adm").getURL());	
			driver.get(System.getProperty(ap.urlPropertyNameAdm));
			return seConnecter(driver);
	}


	/**
	 * Se connecter a Habileo en tant que Encadreur
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageGestionHabilitations connexionEnc(WebDriver driver) throws IOException, Exception{

			System.setProperty(ap.urlPropertyNameEnc,new Environment("enc").getURL());
			driver.get(System.getProperty(ap.urlPropertyNameEnc));
			return seConnecter(driver);
	}
	
	/**
	 * Se connecter a Habileo en tant que Gestionnaire
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageGestionHabilitations connexionGes(WebDriver driver) throws IOException, Exception{

			System.setProperty(ap.urlPropertyNameGes,new Environment("ges").getURL());
			driver.get(System.getProperty(ap.urlPropertyNameGes));
			return seConnecter(driver);
	}
	
	/**
	 * Se connecter a Habileo en tant que Directeur Technique
	 * @param driver
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public static PageGestionHabilitations connexionDt(WebDriver driver) throws IOException, Exception{

		System.setProperty(ap.urlPropertyNameDt,new Environment("dt").getURL());
		driver.get(System.getProperty(ap.urlPropertyNameDt));		
		return seConnecter(driver);
	}
	
	
	
	
	public static void connexionAdm() throws IOException, Exception{
		
		System.setProperty(ap.urlPropertyNameAdm,new Environment("adm").getURL());	
		driver.get(System.getProperty(ap.urlPropertyNameAdm));
	}
//	
//	public static void connexionEnc() throws IOException, Exception{
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//		}
//		System.setProperty(ap.urlPropertyNameEnc,new Environment("enc").getURL());
//		driver.get(System.getProperty(ap.urlPropertyNameEnc));
//	}
//	
	public static PageGestionHabilitations connexionGes() throws IOException, Exception{
		
		System.setProperty(ap.urlPropertyNameGes,new Environment("ges").getURL());
		driver.get(System.getProperty(ap.urlPropertyNameGes));
		
		pI2A=PageFactory.initElements(driver, PageI2A.class);
		pGestionHabilitations=PageFactory.initElements(driver, PageGestionHabilitations.class);
		
		return pGestionHabilitations;
	}
//	

	
	
//	public static void configEnvironnement(String env) throws IOException, Exception{
//	
//	FileInputStream in = new FileInputStream(prefix+"application.properties");
//	Properties prop = new Properties();
//	prop.load(in);
//	in.close();
//	
//	OutputStream output = new FileOutputStream(prefix+"application.properties");
//	prop.setProperty("ta.execution.environment", env);
//	prop.store(output, null);
//	output.close();
//	
//}
	

}