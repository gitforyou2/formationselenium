package main.java.utils;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.junit.Assert;

import main.java.constantes.Constantes;



public class ReadPDF extends ApplicationCommonScripts{


	public String parsedText = null;;
	//String  nom_agent="";
	


	/**
	 * Cette fonction permet de récupérer le text d une fichier pdf
	 * @param nomFichier
	 * @return
	 * @throws IOException
	 */
	public String pdftoText(String nomFichier) throws IOException {
		//String fileName=Constantes.pathTelechargement()+nomFichier;
		String fileName=LOGGER.dossierLogExec.getPath() + "\\"+ nomFichier;


		PDFTextStripper pdfStripper = new PDFTextStripper();
		PDDocument pdDoc = null;
		File file = new File(fileName);
		if (!file.isFile()) {
			LOGGER.info("File " + fileName + " does not exist.");
			return null;
		}
		try {
			pdDoc=PDDocument.load(file);
		} catch (IOException e) {
			LOGGER.info("Unable to open PDF Parser. " + e.getMessage());
			return null;
		}
		try {
			pdfStripper = new PDFTextStripper();
			// int endPage=pdDoc.getNumberOfPages();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(20);
			parsedText = pdfStripper.getText(pdDoc);
			System.out.println(parsedText);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return parsedText;
	}

	/**
	 * Cette fonction permet de supprimer un fichier
	 * @param nomFichier
	 */
	public void supprimerFichier(String nomFichier){
		try{

			File file = new File(Constantes.pathTelechargement()+nomFichier);

			if(file.delete()){
				LOGGER.info("Le fichier - "+file.getName() + " - est supprimé!");
			}else{
				LOGGER.error("Aucun fichier nommé\""  +file.getName() + "\" à supprimer");
			}

		}catch(Exception e){

			e.printStackTrace();

		}
	}
	/**
	 * Cette fonction permet de supprimer un fichier
	 * @param nomFichier
	 */
	public void supprimerFichiers(String nomFichier){
		try{
		
			File[] fichiersASupprimer = new File(Constantes.pathTelechargement()).listFiles();
			
			for(int i = 0; i < fichiersASupprimer.length; i++)
			{
				if(Pattern.matches(nomFichier + "\\d{10}.pdf" ,fichiersASupprimer[i].getName()))
				{
					if(fichiersASupprimer[i].delete()){
						LOGGER.info("Le fichier - "+fichiersASupprimer[i].getName() + " - est supprimé!");
					}else{
						LOGGER.error("Le fichier n existe pas !.");
					}
				}
			}
	
		}catch(Exception e){
	
			e.printStackTrace();
	
		}
	
	}

	/**
	 * Cette fonction permet de déplacer un fichier
	 * @param nomFichier
	 */
	public void deplacerFichier(String nomFichier){
		try{

			File file = new File(Constantes.pathTelechargement()+nomFichier);

			file.renameTo(new File(LOGGER.dossierLogExec.getPath() + "\\" + file.getName()));
			LOGGER.info("Le fichier - "+file.getName() + " - a bien été déplacé!");


		}catch(Exception e){
			LOGGER.error("Le fichier n'a pas été déplacé !.");
			e.printStackTrace();

		}

	}
	
	public void VerifierDonneesRemiseTelephone(Hashtable<String, String> dic){
		verifierIMEI(dic.get("imei"));
		verifierNumTel(dic.get("num_tel"));
		verifierTypeMateriel(dic.get("type_materiel"));
		verifierIdTitulaire(dic.get("id_titulaire"));
		verifierNomTitulaire(dic.get("nom"));
		verifierSiteRattachement(dic.get("site_rattachement"));
		verifierDateRemiseTelephone(dic.get("date_remise"));
		verifierSignature(dic.get("signature"));
		
	}
	

	public void verifierIMEI(String imei){
		
			verifierPresenceDonnee("IMEI");
	}

	public void verifierNumTel(String num_tel){
		verifierPresenceDonnee("Num tél");
	}

	public void verifierTypeMateriel(String type_materiel){
		verifierPresenceDonnee("Type matériel");
	}
	
	public void verifierIdTitulaire(String id_titulaire){
		verifierPresenceDonnee("ID Titulaire");
	}
	
	public void verifierNomTitulaire(String nom){
		verifierPresenceDonnee("Nom");
	}
	
	public void verifierSiteRattachement(String site_rattachement){
		verifierPresenceDonnee("Site de rattachement");
	}
	
	public void verifierDateRemiseTelephone(String date_remise){
		verifierPresenceDonnee("Date de remisedu téléphone");
	}
	
	public void verifierSignature(String signature){
		verifierPresenceDonnee("Signature");
	}

	/**
	 * Cette fonction permet de déplacer un fichier et de lui attribuer un nouveau nom
	 * @param nomFichier
	 * @param nouveauNomFichier
	 */
	public void deplacerFichier(String nomFichier, String nouveauNomFichier){
		try{

			File file = new File(Constantes.pathTelechargement()+nomFichier);

			file.renameTo(new File(LOGGER.dossierLogExec.getPath() + "\\" + nouveauNomFichier));
			LOGGER.info("Le fichier - "+file.getName() + " - a bien été déplacé!");


		}catch(Exception e){
			LOGGER.error("Le fichier n'a pas été déplacé !.");
			e.printStackTrace();

		}

	}



	public void VerifierDonneesParticulierMandat(DataList Liste) throws Exception{

		verifierPresenceDonnee(Liste.getData("Civilite"));
		verifierPresenceDonnee(Liste.getData("Nom"));
		verifierPresenceDonnee(Liste.getData("NomNaissance"));
		verifierPresenceDonnee(Liste.getData("Prenom"));
	}


	public void VerifierDonneesParticulierMandat(Hashtable<String, String> dic){

		verifierPresenceDonnee(dic.get("Civilite"));
		verifierPresenceDonnee(dic.get("Nom"));
		verifierPresenceDonnee(dic.get("NomNaissance"));
		verifierPresenceDonnee(dic.get("Prenom"));
	}



	public void VerifierAdresseMandat(DataList Liste) throws Exception{

		if (parsedText.contains(Liste.getData("NomVoie"))){
			LOGGER.info("L adresse est correct - Nom de la rue");
		} else {
			LOGGER.error("L adresse  est incorrect - Nom de la rue");
			Assert.assertTrue(false);
		}

		if (parsedText.contains(Liste.getData("CP"))){
			LOGGER.info("L adresse est correct - Code Posta");
		} else {
			LOGGER.error("L adresse  est incorrect - Code Posta");
			Assert.assertTrue(false);
		}
	}


	public void VerifierAdresseMandat(Hashtable<String, String> dic){

		if (parsedText.contains(dic.get("NomVoie"))){
			LOGGER.info("L adresse est correct - Nom de la rue");
		} else {
			LOGGER.error("L adresse  est incorrect - Nom de la rue");
			Assert.assertTrue(false);
		}

		if (parsedText.contains(dic.get("CP"))){
			LOGGER.info("L adresse est correct - Code Posta");
		} else {
			LOGGER.error("L adresse  est incorrect - Code Posta");
			Assert.assertTrue(false);
		}
	}


	public void VerifierDonneesEntreprise(DataList Liste) throws Exception{

		verifierTelEntreprise(Liste.getData("TelAvant"));
		verifierTelEntreprise(Liste.getData("TelApres"));
		verifierCourrielEntreprise(Liste.getData("Mail"));
		verifierSiretEntreprise(Liste.getData("Siret"));
		verifierRaisonSocialeEntreprise(Liste.getData("RaisonSociale"));
		verifierFormeJuridiqueEntreprise(Liste.getData("FormeJuridique"));
	}


	public void VerifierDonneesEntreprise(Hashtable<String, String> dic){

		verifierTelEntreprise(dic.get("TelAvant"));
		verifierTelEntreprise(dic.get("TelApres"));
		verifierCourrielEntreprise(dic.get("Mail"));
		verifierSiretEntreprise(dic.get("Siret"));
		verifierRaisonSocialeEntreprise(dic.get("RaisonSociale"));
		verifierFormeJuridiqueEntreprise(dic.get("FormeJuridique"));
	}

	public void VerifierDonneesParticulier(DataList Liste) throws Exception{

		verifierCivilite(Liste.getData("Civilite"));
		verifierNom(Liste.getData("Nom"));
		verifierNomNaissance(Liste.getData("NomNaissance"));
		verifierPrenom(Liste.getData("Prenom"));

		verifiertypeDePiecePID(Liste.getData("TypePDI"));
		verifierNumeroPID(Liste.getData("NumeroPDI"));
		verifierDateDelivrancePID(Liste.getData("DateDelivrancePDI"));
		verifierLieuDelivrancePID(Liste.getData("LieuDelivrancePDI"));
		verifierOrganismeDelivrancePID(Liste.getData("DelivrancePDI"));
		verifierCourriel(Liste.getData("Courriel"));
		verifierNumTelephone(Liste.getData("Telephone"));
	}


	public void VerifierDonneesParticulier(Hashtable<String, String> dic){

		verifierCivilite(dic.get("Civilite"));
		verifierNom(dic.get("Nom"));
		verifierNomNaissance(dic.get("NomNaissance"));
		verifierPrenom(dic.get("Prenom"));

		verifiertypeDePiecePID(dic.get("TypePDI"));
		verifierNumeroPID(dic.get("NumeroPDI"));
		verifierDateDelivrancePID(dic.get("DateDelivrancePDI"));
		verifierLieuDelivrancePID(dic.get("LieuDelivrancePDI"));
		verifierOrganismeDelivrancePID(dic.get("DelivrancePDI"));
		verifierCourriel(dic.get("Courriel"));
		verifierNumTelephone(dic.get("Telephone"));
	}


	public void VerifierAdressePDF(DataList Liste) throws Exception{

		verifierAdresseCP(Liste.getData("CP"));
		verifierAdresseVoie(Liste.getData("NomVoie"));
	}

	public void VerifierAdressePDF(Hashtable<String, String> dic){

		verifierAdresseCP(dic.get("CP"));
		verifierAdresseVoie(dic.get("NomVoie"));
	}

	public void VerifierAdresseLieuDitPDF(Hashtable<String, String> dic){

		String lieuDit = dic.get("LieuDit").toUpperCase();

		if (parsedText.contains("BP ou LIEU DIT "+lieuDit)){
			LOGGER.info("Le Lieu dit est correct");
		} else {
			LOGGER.error("Le Lieu dit est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void VerifierAdresseBureeauArriveePDF(DataList Liste) throws Exception{

		if (parsedText.contains(Liste.getData("Bureau"))){
			LOGGER.info("L adresse de bureau d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de bureau d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}
/*
	public void VerifierStatut(Hashtable<String, String> dic){

		String statutAttendu = "";

		if(dic.get("Statut").equals(Constantes.Supprime))
			statutAttendu = Constantes.ReexpPDFContratSupprimePrefixe + dic.get("DateMAJ") + Constantes.ReexpPDFContratSupprimeSuffixe;
		else if(dic.get("Statut").equals(Constantes.Modifie))
			statutAttendu = Constantes.ReexpPDFContratModifiePrefixe + dic.get("DateMAJ") + Constantes.ReexpPDFContratModifieSuffixe;
		else if(dic.get("Statut").equals(Constantes.Nouveau))
			statutAttendu = Constantes.ReexpPDFContratNouveauPrefixe + dic.get("DateMAJ") + Constantes.ReexpPDFContratNouveauSuffixe;

		if (parsedText.contains(statutAttendu)){
			LOGGER.info("Le Statut: '" + statutAttendu + "' a été trouvé ");
		} 
		else {
			LOGGER.error("le Statut: '" + statutAttendu + "' n'a pas été trouvé ");
			Assert.assertTrue(false);
		}
	}*/




	public void VerifierAdresseBureauArriveePDF(Hashtable<String, String> dic){

		if (parsedText.contains(dic.get("Bureau"))){
			LOGGER.info("L adresse de bureau d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de bureau d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void VerifierAdressePaysArriveePDF(DataList Liste) throws Exception{

		if (parsedText.contains(Liste.getData("Pays"))){
			LOGGER.info("L adresse de Pays d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de Pays d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void VerifierAdressePaysArriveePDF(Hashtable<String, String> dic){

		if (parsedText.contains(dic.get("Pays"))){
			LOGGER.info("L adresse de Pays d 'arrivee est correct");
		} else {
			LOGGER.error("L adresse de Pays d arrivee est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierCivilite(String civilite){

		if (parsedText.contains(civilite+" Nom")){
			LOGGER.info("La civilite est correct");
		} else {
			LOGGER.error("La civilite est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierNom(String nom){
		nom=nom.toUpperCase();
		if (parsedText.contains("Nom : "+nom)){
			LOGGER.info("Le nom est correct");
		} else {
			LOGGER.error("Le nom est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierNomNaissance(String nomNaissance){
		nomNaissance=nomNaissance.toUpperCase();
		if (parsedText.contains(nomNaissance)||parsedText.contains(nomNaissance)){
			LOGGER.info("Le nom de naissance est correct");
		} else {
			LOGGER.error("Le nom de naissance est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierPrenom(String prenom){
		prenom=prenom.toUpperCase();
		if (parsedText.contains("Prénom : "+prenom)){
			LOGGER.info("Le prenom est correct");
		} else {
			LOGGER.error("Le prenom est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifiertypeDePiecePID(String piece){

		if (parsedText.contains("pièce : "+piece+" n°")){
			LOGGER.info("Le type de la piece d identite est correct");
		} else {
			LOGGER.error("Le type de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierNumeroPID(String numero){

		if (parsedText.contains("n° "+numero)){
			LOGGER.info("Le numero de la piece d identite est correct");
		} else {
			LOGGER.error("Le numero de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierDateDelivrancePID(String date){

		if (parsedText.contains("le : "+date)){
			LOGGER.info("La date de delivrance de la piece d identite est correcte");
		} else {
			LOGGER.error("La date de delivrance de la piece d identite est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierLieuDelivrancePID(String lieu){
		lieu=lieu.toUpperCase();
		if (parsedText.contains("à : "+lieu)){
			LOGGER.info("Le lieu de delivrance de la piece d identite est correct");
		} else {
			LOGGER.error("Le lieu de delivrance de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierOrganismeDelivrancePID(String organisme){

		if (parsedText.contains("Par : "+organisme)){
			LOGGER.info("L organisme de delivrance de la piece d identite est correct");
		} else {
			LOGGER.error("L organisme de delivrance de la piece d identite est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierCourriel(String mail){

		if (parsedText.contains(mail)){
			LOGGER.info("L adresse email est correcte");
		} else {
			LOGGER.error("L adresse email est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierNumTelephone(String num){
		if (!num.equals(""))
			num=num.substring(0, 2)+'.'+num.substring(2, 4)+'.'+num.substring(4, 6)+'.'+num.substring(6, 8)+'.'+num.substring(8, 10);
		if (parsedText.contains("phone : "+num)){
			LOGGER.info("Le numero de telephone est correct");
		} else {
			LOGGER.error("Le numero de telephone est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierAdresseCP(String cp){

		if (parsedText.contains("LOCALITE "+cp)){
			LOGGER.info("Le CP est correct");
		} else {
			LOGGER.error("Le CP est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierAdresseVoie(String voie){

		if (parsedText.contains(voie)){
			LOGGER.info("Le nom de la voie est correct");
		} else {
			LOGGER.error("Le nom de la voie est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierTelEntreprise(String num){
		if (!num.equals(""))
			num=num.substring(0, 2)+'.'+num.substring(2, 4)+'.'+num.substring(4, 6)+'.'+num.substring(6, 8)+'.'+num.substring(8, 10);
		if (parsedText.contains("adresse : "+num)){
			LOGGER.info("Le numero de telephone est correct");
		} else {
			LOGGER.error("Le numero de telephone est incorrect");
			Assert.assertTrue(false);
		}
	}
	public void verifierCourrielEntreprise(String mail){

		if (parsedText.contains("E-mail * : "+mail)){
			LOGGER.info("L adresse email est correcte");
		} else {
			LOGGER.error("L adresse email est incorrecte");
			Assert.assertTrue(false);
		}
	}

	public void verifierSiretEntreprise(String code){

		if (parsedText.contains(code)){
			LOGGER.info("Le Code SIRET est correct");
		} else {
			LOGGER.error("Le Code SIRET est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierRaisonSocialeEntreprise(String raison){

		if (parsedText.contains(raison)){
			LOGGER.info("Le Raison Sociale est correct");
		} else {
			LOGGER.error("Le Raison Sociale est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierFormeJuridiqueEntreprise(String forme){

		if (parsedText.contains("Forme juridique : "+forme)){
			LOGGER.info("Le Forme Juridique est correct");
		} else {
			LOGGER.error("Le Forme Juridique est incorrect");
			Assert.assertTrue(false);
		}
	}

	public void verifierPresenceDonnee(String donnee){
		//donnee=donnee.toUpperCase();
		if (parsedText.contains(donnee)){
			LOGGER.info("Le donnee "+donnee+" est correct");
		} else {
			LOGGER.error("Le donnee "+donnee+" est incorrect");
			Assert.assertTrue(false);
		}
	}
	public void VerifierDonneesAccuseRestitution(){
		
		verifierChampNomVide("Monsieur, Madame :...");
		verifierChampQualite("agissant en qualité de :");
		verifierChampMarque("Type/marque de l’appareil : ...");
		verifierTypeAbonnement("Type d’abonnement : FACTEO");
		
	}
	
	public void verifierChampNomVide(String champ_nom){
		if (parsedText.contains(champ_nom)){
			LOGGER.info("le texte du champ nom est correcte avec un contenu vide");
		} else {
			LOGGER.error("le texte du champ nom est incorrect");
			Assert.assertTrue(false);
		}
	}
	
	public void verifierChampQualite(String champ_qualite){
		if (parsedText.contains(champ_qualite)){
			LOGGER.info("le texte du champ qualité est correcte avec un contenu vide");
		} else {
			LOGGER.error("le texte du champ qualité est incorrect");
			Assert.assertTrue(false);
		}
	}
	
	public void verifierTypeAbonnement(String type_abonnement){
		if (parsedText.contains(type_abonnement)){
			LOGGER.info("le texte du champ type abonnement est correcte avec le contenu Facteo");
		} else {
			LOGGER.error("le texte du champ type abonnement est incorrect");
			Assert.assertTrue(false);
		}
	}
	
	public void verifierChampMarque(String champ_marque){
		if (parsedText.contains(champ_marque)){
			LOGGER.info("le texte du champ marque est correcte avec un contenu vide");
		} else {
			LOGGER.error("le texte du champ marque est incorrect");
			Assert.assertTrue(false);
		}
	}
	
	public void verifierChampNomPrenom(String champ_nom_prenom, String nom){
	if (parsedText.contains(champ_nom_prenom+" "+ nom)) {
			LOGGER.info("le texte du champ nom_prenom est correcte avec du contenu");
		} else {
			LOGGER.error("le texte du champ nom_prenom est incorrect");
			Assert.assertTrue(false);
		}
	}
	public void verifierPresenceAdresseLaposte(String champ_nom_prenom, String nom){
	if (parsedText.contains(champ_nom_prenom+" "+ nom)) {
			LOGGER.info("le texte du champ nom_prenom est correcte avec du contenu");
		} else {
			LOGGER.error("le texte du champ nom_prenom est incorrect");
			Assert.assertTrue(false);
		}
	}
	
	public void verifierImei(String num_serie, String imei){
	if (parsedText.contains(num_serie+" "+ imei)) {
			LOGGER.info("le texte du champ num_serie est correcte avec du contenu");
		} else {
			LOGGER.error("le texte du champ num_serie est incorrect");
			Assert.assertTrue(false);
		}
	}
	
	public void verifierIdentifiantLaPoste(String nAdresseLaposte){
	if (parsedText.contains(nAdresseLaposte)) {
			LOGGER.info("le texte @laposte.fr est bien présent");
		} else {
			LOGGER.error("le texte @laposte.fr n'est pas présent");
			Assert.assertTrue(true);
		}
	}
	
	public void verifierAdresseEditer(String mail,String file){
		
		String txt=file.substring(0, file.lastIndexOf(""));			
		if(txt.contains(mail))
			LOGGER.info("le texte "+mail+" est bien présent");
		else 
			LOGGER.error("le texte @laposte.fr n'est pas présent");
	}
}