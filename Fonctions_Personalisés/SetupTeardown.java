package main.java.utils;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import main.java.pages.PageI2A;
import main.java.pages.PageSupportSI;
import main.java.pages.Google.PageCompteGoogle;
import main.java.pages.Habileo.AgentsFacteo.PageConsulterListeAgents;
import main.java.pages.Habileo.AgentsFacteo.PageDeclarerAgentsFacteo;
import main.java.pages.Habileo.AgentsFacteo.PageDeclarerDesDemandesEnMasseSurLesAgents;
import main.java.pages.Habileo.AgentsFacteo.PageFicheAgents;
import main.java.pages.Habileo.RechercheEtPilotage.PageExport;
import main.java.pages.Habileo.RechercheEtPilotage.PagePilotageEtIndicateurs;
import main.java.pages.Habileo.RechercheEtPilotage.PageRechercheGenerale;
import main.java.pages.Habileo.RechercheEtPilotage.PageRechercheSite;
import main.java.pages.Habileo.SitesFacteo.PageConsulterListeSites;
import main.java.pages.Habileo.SitesFacteo.PageDeclarerSitesFacteo;
import main.java.pages.Habileo.SitesFacteo.PageFicheSites;
import main.java.pages.Habileo.TelephonesFacteo.PageAffectationsTemporaires;
import main.java.pages.Habileo.TelephonesFacteo.PageAffecterTelephonesDispo;
import main.java.pages.Habileo.TelephonesFacteo.PageConsulterListeTelAffectes;
import main.java.pages.Habileo.TelephonesFacteo.PageConsulterListeTelHorsTEM;
import main.java.pages.Habileo.TelephonesFacteo.PageDeclarerDesDemandesEnMasseTelephones;
import main.java.pages.Habileo.TelephonesFacteo.PageFicheTelephone;
import main.java.pages.Habileo.TraitementsServeurs.PageConfigurationBouchons;
import main.java.pages.Habileo.TraitementsServeurs.PageEditionDesCGU;
import main.java.pages.Habileo.TraitementsServeurs.PageSystemesExternes;
import main.java.pages.Habileo.TraitementsServeurs.PageTraitementsAuto;
import main.java.pages.PageGestionHabilitations.PageGestionHabilitations;
import main.java.pages.Pradeo.PagePradeo;
import main.java.pages.StoreAppaloosa.PageChangerMDP;
import main.java.pages.StoreAppaloosa.PageCompteStore;
import main.java.pages.TEM.PageActeGesion;
import main.java.pages.TEM.PageDAPORemplacements;
import main.java.pages.TEM.PageEquipements;
import main.java.pages.TEM.PageSuiviCampagnesRenouvellement;
import main.java.pages.TEM.PageTEM;
import test.PreparationJDD.PreparationJDDTEM;

@SuppressWarnings("rawtypes")
public class SetupTeardown extends ApplicationCommonScripts{
	
	public PageGestionHabilitations pGestionHabilitations;
	public PreparationJDDTEM preparationJDDTEM;
	public PageSupportSI pSupportSI;
	public PageConsulterListeAgents pConsulterAgentsFacteo;
	public PageTraitementsAuto pTraitementsAuto;
	public PageSystemesExternes pSystemesExternes;
	public PageFicheAgents pFicheAgent;
	public PageConfigurationBouchons pConfigurationBouchons;
	public PageDeclarerAgentsFacteo pDeclarerAgentsFacteo;
	public PageConsulterListeSites pConsulterSitesFacteo;
	public PageFicheSites pFicheSite;
	public PageCompteGoogle pCompteGoogle;
	public PageI2A pI2A;
	public PageFicheTelephone pFicheTelephone;
	public PageDeclarerSitesFacteo pDeclarerSitesFacteo;
	public PageTEM pTEM;
	public PageExport pageExport;
	public PageConsulterListeTelAffectes PConsulterListeTelAffectes;
	public PageConsulterListeTelHorsTEM PConsulterListeTelHorsTEM;
	public PageDeclarerDesDemandesEnMasseTelephones pDeclarerDesDemandesEnMasseTelephones;
	public PageAffecterTelephonesDispo pAffecterTelephonesDispo;
	public PageSuiviCampagnesRenouvellement PSuiviCampagnesRenouvellement;
	public PageDAPORemplacements pageDAPORemplacements;
	public PageActeGesion pageActeGesion;
	public PageEquipements pEquipements;
	public PageDeclarerDesDemandesEnMasseSurLesAgents pDeclarerDesDemandesEnMasseSurLesAgents;
	public PageAffectationsTemporaires pAffectationsTemporaires;
	
	public PageCompteStore pCompteStore;
	public PageChangerMDP pChangerMDP;
	public PagePradeo pPradeo;
	public PageEditionDesCGU PEditionDesCGU;
	public PageRechercheGenerale pRechercheGenerale;
	public PageRechercheSite pRechercheSite;
	public static final String xlsx = "xlsx";
	public static final String csv = "csv";
	public static final String status="status";
	public PageConsulterListeSites pConsulterListeSite;
	
	public PagePilotageEtIndicateurs pPilotageIndicateur;
	
	@BeforeMethod
	public void setup() throws Exception, IOException, Exception{
		logStart(this.getClass().getSimpleName());
		
		//Ouverture du navigateur
//		driver = new Browser().getDriver();
//		driver.manage().window().maximize();
		driver=ConfigConnexion.initialisationDriver();
		String domain;
		domain = System.getenv("USERDOMAIN");
		driver.manage().window().maximize();

		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
		html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		
		//Ouverture de l'application
//System.setProperty(ap.urlPropertyName,new Environment(status).getURL());
//		driver.get(System.getProperty(ap.urlPropertyName));
//		pSupportSI = PageFactory.initElements(driver, PageSupportSI.class);
//		pSupportSI.cliquerLienSupportSI();
//		PageI2A pI2A = pSupportSI.cliquerLienAccostage();
//		PageI2AGestion pI2AGestion = pI2A.seConnecter();
//		pGestionHabilitations = PageFactory.initElements(driver, PageGestionHabilitations.class);
//		pGestionHabilitations = pI2AGestion.cliquerLienEnvironnement();
		//fermerAlert();
	}
	
	@AfterMethod
	public void teardown(){
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		try {
			SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");//dd/MM/yyyy
		    Date now = new Date();
		    String strDate = sdfDate.format(now);
			FileUtils.copyFile(scrFile, new File(System.getenv("TEMP") + "\\HABILEO_" + strDate + "_" + this.getClass().getSimpleName() + "_" + ".png"));
		} catch (IOException e) {
			//Auto-generated catch block
			e.printStackTrace();
		}
		String fermerNavig = System.getProperty("fermerNavig");
			if(fermerNavig.equals("false"))
				closeBrowser();
	}
	public static void closeBrowser(){
		if (driver != null) {
			driver.quit();
	 
	        //Runtime.getRuntime().exec("taskkill /IM geckodriver.exe /F /t" );
	    }
	    
	}
	
}