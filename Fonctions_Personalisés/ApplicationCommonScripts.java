package main.java.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;

public class ApplicationCommonScripts { //ref
	// ==========================================================================================
	// DECLARATIONS
	// ==========================================================================================
	// Log
	//protected static final Logger LOGGER = LoggerFactory.getLogger(ApplicationCommonScripts.class);
	protected static Log LOGGER;
	//private final String sep = "==========";
	@SuppressWarnings("unused")
	private final String sep = "";
	protected static final String LF = System.getProperty("line.separator");
	public static final long DEFAULT_TIME_OUT_IN_SECONDS = 30;
	public static final long DEFAULT_MISSING_TIME_OUT_IN_SECONDS = 10;
	public static final long DEFAULT_SHORT_TIME_OUT_IN_SECONDS = 4;
	
	// Parametres de l'application
	protected static AppParameters ap = new AppParameters();
	protected static DataList listeParametre = new DataList(ap.propertiesFile);

	// WebDriver
	public static WebDriver driver;
	public WebDriverWait wait;

	//Androiddriver
	//protected AppiumDriver<AndroidElement> driverMobile;

	// ==========================================================================================
	// METHODES
	// ==========================================================================================
	// ====================
	// Log
	// ====================
	public void logStart(String testName) {

		LOGGER = new Log(testName);
		LOGGER.infoStart("Debut du test - " + testName);

	}

	public void logStart(String className, String testName) {

		LOGGER = new Log(className, testName);
		LOGGER.infoStart("Debut du test - " + testName);

	}


	public void logPDT(String pasDeTest) {
		LOGGER.info(pasDeTest);
	}

	// ====================
	// Utilitaires
	// ====================


	//---------------------------------------------------
	// VISIBILITE
	//---------------------------------------------------

	//	@Deprecated
	//	/**
	//	 * Verification sur la visibilité d'un element
	//	 * @param elementLocator
	//	 * @param elementName
	//	 * Nom sous lequel l'element apparaîtra dans le log
	//	 * @param logIfTrue
	//	 * True: si l'element est visible la verification apparaîtra dans les log
	//	 * False: si l'element est visible la verification n'apparaitra pas dans les log
	//	 * @return
	//	 */
	//	public boolean verifierVisibiliteElement(By elementLocator, String elementName, Boolean logIfTrue) {
	//		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
	//		String msgIfTrue = "L'element " + elementName + " est bien visible.";
	//		String msgIfFalse = "L'element " + elementName + " n'est pas visible.";
	//		try {
	//			wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
	//			if (logIfTrue == true) {
	//				logPDT("Verification sur la visibilité de l'element " + elementName + ".");
	//				LOGGER.info(msgIfTrue);
	//			}
	//			return true;
	//		} catch (TimeoutException t) {
	//			LOGGER.error(msgIfFalse);
	//			throw new NoSuchElementException(msgIfFalse);
	//		}
	//	}

	/**
	 * Vérifie qu'un élément est bien visible
	 * 
	 * @param elementLocator = element que l'on veut vérifier
	 * @param logIfTrue 
	 *            <li>True  : Si l'élément est visible la vérification apparaîtra dans les log 
	 *            <li>False : Si l'élément est visible la vérification n'apparaitra pas dans les log
	 * @param msgIfTrue  = Message qui sera affiché si l'élément est bien visible et si logIfTrue a pour valeur True
	 * @param msgIfFalse = Message qui sera affiché si l'élément est absent ou invisible
	 * 
	 * @return WebElement = Element visible
	 */
	private WebElement verifierVisibiliteElement(By elementLocator, boolean logIfTrue, String msgIfTrue, String msgIfFalse) {
		int nbt = 0; //nb attente Requete ajax
		WebElement element = null; 
		LOGGER.debug("Vérification visibilité élément : " + elementLocator);
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		wait.withMessage(msgIfFalse);

		try {
			AjaxEnCours();

			element = wait.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
			if (logIfTrue == true)
				LOGGER.info(msgIfTrue);

		} catch (TimeoutException t) {
			LOGGER.info(msgIfFalse);
			LOGGER.error("Erreur : " + elementLocator.toString());
			throw new NoSuchElementException(msgIfFalse);
		}
		return element;
	}

	/**
	 * Vérifie qu'un élément est bien visible. Si l'élément est bien visible,
	 * aucun message n'est affiché dans les logs.
	 * 
	 * @param elementLocator
	 * @param msgIfFalse = Message customisé qui sera affiché si l'élément est absent ou invisible.
	 * @see verifierVisibiliteElement
	 */
	public WebElement verifierVisibiliteElement(By elementLocator, String msgIfFalse) {
		String msgIfTrue = "";
		return verifierVisibiliteElement(elementLocator, false, msgIfTrue, msgIfFalse);
	}

	/**
	 * Vérifie qu'un élément est bien visible
	 * 
	 * @param elementLocator
	 * @param elementName = Nom de l'élément dont on veut vérifier la visibilité
	 * @param logIfTrue <br>
	 * <li>	True  : Si l'élément est visible, la vérification apparaîtra dans les log <br>
	 * <li>	False : Si l'élément est visible, la vérification n'apparaitra pas dans les log
	 */
	public WebElement verifierVisibiliteElement(By elementLocator, String elementName, boolean logIfTrue) {
		String msgIfTrue = "L'élément " + elementName + " est bien présent et visible.";
		String msgIfFalse = "L'élément " + elementName + " est absent ou invisible.";

		return verifierVisibiliteElement(elementLocator, logIfTrue, msgIfTrue, msgIfFalse);
	}

	//EN DOUBLON avec verifierVisibiliteElement (05/10/2017 à supprimer)
	/**
	 * Vérifie qu'un élément est bien visible
	 * 
	 * @param elementLocator
	 * @param elementName = Nom de l'élément dont on veut vérifier la visibilité
	 * @param logIfTrue   <li> True  : Si l'élément est visible, la vérification apparaitra dans les log 
	 *                    <li> False : Si l'élément est visible, la vérification n'apparaitra pas dans les log
	 * @deprecated  verifierVisibiliteElement
	 */
	public WebElement verifierVisibiliteElement(By elementLocator, String regate, String elementName, boolean logIfTrue) {
		String msgIfTrue = "L'élément " + elementName + " est bien présent et visible.";
		String msgIfFalse = "L'élément " + elementName + " est absent ou invisible.";

		return verifierVisibiliteElement(elementLocator, logIfTrue, msgIfTrue, msgIfFalse);
	}
	

	/**
	 * Vérifie qu'un élément est bien visible. Si l'élément est bien visible, un message est affiché dans les logs.
	 * @param elementLocator
	 * @param msgIfTrue  = Message qui sera affiché si l'élément est bien visible
	 * @param msgIfFalse = Message qui sera affiché si l'élément est absent ou invisible
	 * @return WebElement : Element visible
	 * @see verifierVisibiliteElement
	 */
	public WebElement verifierVisibiliteElement(By elementLocator, String msgIfTrue, String msgIfFalse) {
		return verifierVisibiliteElement(elementLocator, true, msgIfTrue, msgIfFalse);
	}

	/**
	 * Vérifie qu'un élément est invisible.
	 * 
	 * @param elementLocator
	 * @param logIfTrue 
	 *			<li> True  : Si l'élément est invisible, la vérification apparaitra dans les log. 
	 * 			<li> False : Si l'élément est invisible, la vérification n'apparaitra pas dans les log.
	 * @param msgIfTrue  = Message qui sera affiché si l'élément est invisible et si logIfTrue a pour valeur True.
	 * @param msgIfFalse = Message qui sera affiché si l'élément est visible.
	 */
	private void verifierInvisibiliteElement(By elementLocator, boolean logIfTrue, String msgIfTrue, String msgIfFalse) {
		
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		wait.withMessage(msgIfFalse);
		try {
			AjaxEnCours();
			wait.until(ExpectedConditions.invisibilityOfElementLocated(elementLocator));
			if (logIfTrue == true) {
				LOGGER.info(msgIfTrue);
			}
		} catch (TimeoutException t) {
			LOGGER.info(msgIfFalse);
			LOGGER.error(elementLocator.toString());
			throw new NoSuchElementException(msgIfFalse);
		}
	}
	/**
	 * Attendre le chargement d'ajax
	 */
	protected static void AjaxEnCours() {
		int nbt = 0; //nb attente Requete ajax
		Boolean isJqueryUsed = (Boolean) ((JavascriptExecutor) driver).executeScript("return (typeof(jQuery) != 'undefined')");
		Boolean isJsUsed = (Boolean) ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
		if (isJqueryUsed && isJsUsed) {
			while (true) {
				// JavaScript test to verify jQuery is active or not
				Boolean ajaxIsComplete = (Boolean) (((JavascriptExecutor) driver)
						.executeScript("return jQuery.active == 0"));
				if (ajaxIsComplete)
					break;
				nbt++;
				try {
					LOGGER.debug("- Requete Ajax encours : " + nbt);
					attendre(100);
				} catch (Exception e) {
				}
			}
		}
	}

	/**
	 * Vérifie qu'un élément est invisible. 
	 * Si l'élément est invisible, aucun message n'est affiché dans les logs.
	 * 
	 * @param elementLocator
	 * @param msgIfFalse = Message qui sera affiché si l'élément est visible.
	 * @see verifierInvisibiliteElement
	 */
	public void verifierInvisibiliteElement(By elementLocator, String msgIfFalse) {
		verifierInvisibiliteElement(elementLocator, false, null, msgIfFalse);
	}

	/**
	 * Vérifie qu'un élément est invisible.
	 * 
	 * @param elementLocator
	 * @param elementName = Nom de l'élément dont on veut vérifier l'invisibilité.
	 * @param logIfTrue
	 *            <li>True: Si l'élément est invisible la vérification apparaitra dans les logs. 
	 *            <li>False: Si l'élément est invisible la vérification n'apparaitra pas dans les logs.
	 * @see verifierInvisibiliteElement
	 */
	public void verifierInvisibiliteElement(By elementLocator, String elementName, boolean logIfTrue) {
		String msgIfTrue = "L'élément " + elementName + " est bien invisible.";
		String msgIfFalse = "L'élément " + elementName + " est visible (à tort).";
		verifierInvisibiliteElement(elementLocator, logIfTrue, msgIfTrue, msgIfFalse);
	}

	/**
	 * Vérifie qu'un élément est invisible. Si l'élément est invisible, un
	 * message est affiché dans les logs.
	 * 
	 * @param elementLocator
	 * @param msgIfTrue  = Message qui sera affiché si l'élément est invisible.
	 * @param msgIfFalse = Message qui sera affiché si l'élément est visible.
	 * @see verifierInvisibiliteElement
	 */
	public void verifierInvisibiliteElement(By elementLocator, String msgIfTrue, String msgIfFalse) {
		verifierInvisibiliteElement(elementLocator, true, msgIfTrue, msgIfFalse);
	}

	//---------------------------------------------------
	// CLIQUABLE
	//---------------------------------------------------

	/**
	 * Vérifier qu'un élément est bien cliquable
	 * 
	 * @param elementLocator
	 * @param elementName
	 *            Nom sous lequel l'element apparaîtra dans le log
	 * @param logIfTrue
	 *          <li> True: si l'element est cliquable la verification apparaîtra dans les log 
	 *          <li> False: si l'element est cliquable la verification n'apparaitra pas dans les log
	 * @return boolean
	 */
	public boolean verifierElementCliquable(By elementLocator, String elementName, boolean logIfTrue) {
		String msgIfTrue = "L'element " + elementName + " est bien cliquable.";
		String msgIfFalse = "L'element " + elementName + " n'est pas cliquable.";
		AjaxEnCours();
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);

		try {
			wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
			if (logIfTrue == true) {
				logPDT("Verification sur l'element " + elementName + " cliquable.");
				LOGGER.info(msgIfTrue);
			}
		} catch (TimeoutException t) {
			LOGGER.info(msgIfFalse);
			LOGGER.error(elementLocator.toString());
			throw new NoSuchElementException(msgIfFalse);
		}
		return true;
	}

	public boolean verifierElementCliquablebool(By elementLocator, String elementName, boolean logIfTrue) {
		String msgIfTrue = "L'element " + elementName + " est bien cliquable.";
		String msgIfFalse = "L'element " + elementName + " n'est pas cliquable.";
		AjaxEnCours();
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);

		try {
			wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
			if (logIfTrue == true) {
				logPDT("Verification sur l'element " + elementName + " cliquable.");
				LOGGER.info(msgIfTrue);
			}
		} catch (TimeoutException t) {
			LOGGER.info(msgIfFalse);
			LOGGER.error(elementLocator.toString());
			return false;
		}
		return true;
	}

	
	
	/**
	 * Vérifier qu'un élément est bien cliquable
	 * 
	 * @param elementLocator
	 * @param elementName
	 * @return boolean
	 */
	public boolean verifierAbsenceElement(By elementLocator, String elementName, boolean logIfTrue) {
		String msgIfTrue = "L'element " + elementName + " est bien absent.";
		String msgIfFalse = "L'element " + elementName + " est present.";
		AjaxEnCours();
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		wait.withMessage(msgIfFalse);
		LOGGER.debug("Vérification sur la présence de l'élément " + elementName + ".");
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(elementLocator));
			if (logIfTrue == true) {
				LOGGER.info(msgIfTrue);
			}
			return true;
		} catch (TimeoutException t) {
			LOGGER.error(msgIfFalse);
			throw new NoSuchElementException(msgIfFalse);
		}
	}

	/**
	 * click sur l'élément après avoir verifierVisibiliteElement et verifierElementCliquable
	 * 
	 * @param elementLocator : By de l'élément à cliquer
	 * @param elementName : String contenant le nom de l'élément (pour le log)
	 */
	public void cliquerElement(By elementLocator, String elementName) {
		LOGGER.debug("Clic sur " + elementName);

		attendre(300);
		verifierVisibiliteElement(elementLocator, elementName, false);
		verifierElementCliquable(elementLocator, elementName, false);
	//	driver.manage().timeouts().pageLoadTimeout(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
	//	driver.manage().timeouts().implicitlyWait(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
		WebElement element = driver.findElement(elementLocator);
		element.click();
		AjaxEnCours();
		attendre(300);
	}

	/**
	 * click sur l'élément après avoir verifierVisibiliteElement et verifierElementCliquable
	 * 
	 * @param elementLocator : By de l'élément à cliquer
	 * @param elementName : String contenant le nom de l'élément (pour le log)
	 * @param logIfTrue 
	 *          <li> True: si l'element est cliquable la verification apparaîtra dans les log 
	 *          <li> False: si l'element est cliquable la verification n'apparaitra pas dans les log
	 */
	public void cliquerElement(By elementLocator, String elementName, Boolean logIfTrue) {
		LOGGER.debug("Clic sur " + elementName);

		attendre(300);
		AjaxEnCours();
		verifierVisibiliteElement(elementLocator, elementName, false);
		verifierElementCliquable(elementLocator, elementName, false);
//		driver.manage().timeouts().pageLoadTimeout(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
//	    driver.manage().timeouts().implicitlyWait(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
		driver.findElement(elementLocator).click();
		AjaxEnCours();
		attendre(300);
	}
	
	
	protected String scrollTo(String classBar, String plusOuMoinsPixels) {
		String script = "$('%s').mCustomScrollbar('scrollTo','%s')";
		return String.format(script, classBar, plusOuMoinsPixels);
	}
	
	/**
	 * Cette méthode permet de scroller jusqu'à l'element
	 */	
	public void scrollToElement(By element) {
		 WebElement Element = driver.findElement(element);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", Element);
	}

	/**
	 * Attendre (wait) 
	 * 
	 * @param milliSeconde Temps en millisecond 
	 */
	public static void attendre(int milliSeconde) {
		try {
			Thread.sleep(milliSeconde);
		} catch (InterruptedException e) {
			LOGGER.warn("La pause de " + milliSeconde + "ms ne s'est pas effectuée.");
		}
	}

	/**
	 * Attendre (wait) 
	 * 
	 * @param milliSeconde Temps en millisecond
	 * @param logIfTrue	 
	 *          <li> True : msg apparaîtra dans les log 
	 *          <li> False: msg n'apparaitra pas dans les log 
	 */
	public void attendre(int milliSeconde, Boolean logIfTrue) {
		if (logIfTrue) {
			LOGGER.info("Pause de " + milliSeconde + "ms.");
		}
		try {
			Thread.sleep(milliSeconde);
		} catch (InterruptedException e) {
			LOGGER.warn("La pause de " + milliSeconde + "ms ne s'est pas effectuée.");
		}
	}

	/**
	 * Scroll
	 * @param scrollBar
	 * @param nbScroll
	 * @param scroll
	 * @param xpathExpression
	 */
	public void parcourirListeJS(String scrollBar, int nbScroll, String scroll, String xpathExpression) {
		JavascriptExecutor jse = null;
		if (driver instanceof JavascriptExecutor) {
			jse = (JavascriptExecutor) driver;
		}

		for (int t = 1; t < nbScroll; t++) {
			// LOG.warn("test " + t);
			try {
				if (driver.findElement(By.xpath(xpathExpression)).isDisplayed()) {
					// LOG.warn("Trouve");
					break;
				} else {
					// LOG.warn("scroll down");
					jse.executeScript(scrollTo(scrollBar, scroll));
				}
			} catch (NoSuchElementException e) {
				// LOG.warn("scroll down");
				jse.executeScript(scrollTo(scrollBar, scroll));
			}

			attendre(500);

		}

	}

	/**
	 * Verifie l'URL d'une page qui s'ouvre dans un nouvel onglet
	 * 
	 * @param urlBase
	 * @param lien
	 */
	public void cliquerLienExterne(final String urlBase, By lien) {
		ArrayList<String> tab = null;
		cliquerElement(lien, "lien " + urlBase);
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					if (driver.getWindowHandles().size() > 1) {
						return true;
					} else {
						return false;
					}
				}
			});
			tab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tab.get(1));
			wait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					if (driver.getCurrentUrl().contains(urlBase)) {
						return true;
					} else {
						return false;
					}
				}
			});
		} catch (TimeoutException e) {
			String reason = "La page " + urlBase + " ne s'est pas affichée.";
			LOGGER.error(reason);
			throw new NoSuchElementException(reason);
		}
		driver.close();
		driver.switchTo().window(tab.get(0));
		LOGGER.debug("La page " + urlBase + " s'est bien affichée.");
	}

	/**
	 * Vérifie l'url et la presence d'un element d'une page s'ouvrant dans un nouvel onglet
	 * 
	 * @param urlBase
	 * @param xpathElement
	 * @param lien
	 */
	public void cliquerLienExterne(final String nomLien, final By element, By lien) {
		ArrayList<String> tab = null;
		cliquerElement(lien, "lien " + nomLien);
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					if (driver.getWindowHandles().size() > 1) {
						return true;
					} else {
						return false;
					}
				}
			});
			tab = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tab.get(1));
			wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		} catch (TimeoutException e) {
			String reason = "La page " + nomLien + " ne s'est pas affichee.";
			LOGGER.error(reason);
			throw new NoSuchElementException(reason);
		}
		driver.close();
		driver.switchTo().window(tab.get(0));
		LOGGER.debug("La page " + nomLien + " s'est bien affichee.");
	}

	public void effacerContenuFichierTxt(String path) throws IOException {
		new FileWriter(new File(path)).close();
	}

	public String logAutoit(String path) throws IOException {
		String log = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));

			try {
				String line;
				line = br.readLine();
				System.out.print(line);
				log = line;
				br.close();
			} catch (IOException exception) {

			}

		} catch (FileNotFoundException exception) {

		}

		return log;
	}

	public void verifierLogAutoitF(String log, String logAttendu, String msgIfTrue, String msgIfFalse) {
		if (log.equalsIgnoreCase(logAttendu)) {
			LOGGER.info(msgIfTrue);

		} else {
			LOGGER.error(msgIfFalse);
			throw new NoSuchElementException(msgIfFalse);
		}
	}



	//---------------------------------------------------
	// PRESENCE
	//---------------------------------------------------

	//
	// /**
	// * Vérifie qu'un element est bien present
	// * @param elementLocator
	// * @param elementName
	// * Nom sous lequel l'element apparaîtra dans le log
	// * @param logIfTrue
	// * True: si l'element est present la verification apparaîtra dans les log
	// * False: si l'element est present la verification n'apparaitra pas dans
	// les log
	// * @return
	// */
	// public boolean verifierPresenceElement(By elementLocator, String
	// elementName, boolean logIfTrue){
	// String msgIfTrue = "L'element " + elementName + " est bien present.";
	// String msgIfFalse = "L'element " + elementName + " n'est pas present.";
	// wait.withMessage(msgIfFalse);
	//
	// try{
	// wait.until(ExpectedConditions.presenceOfElementLocated(elementLocator));
	// if(logIfTrue == true){
	// logPDT("Verification sur la presence de l'element " + elementName + ".");
	// LOGGER.info(msgIfTrue);
	// }
	// return true;
	// }catch(TimeoutException t){
	// LOGGER.error(msgIfFalse);
	// throw new NoSuchElementException(msgIfFalse);
	// }
	// }

	/**
	 * Vérifie qu'un élément est bien présent
	 * 
	 * @param elementLocator
	 * @param logIfTrue
	 *            <li>True: si l'élément est présent la vérification apparaitra dans les log 
	 *            <li>False: si l'élément est présent la vérification n'apparaitra pas dans les log
	 * @param msgIfTrue
	 *            Message qui sera affiché si l'élément est bien présent et si logIfTrue a pour valeur True
	 * @param msgIfFalse
	 *            Message qui sera affiché si l'élément est absent
	 */
	private void verifierPresenceElement(By elementLocator, boolean logIfTrue, String msgIfTrue, String msgIfFalse) {
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
		wait.withMessage(msgIfFalse);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(elementLocator));
			if (logIfTrue == true) {
				LOGGER.info(msgIfTrue);
			}
		} catch (TimeoutException t) {
			LOGGER.error(msgIfFalse);
			LOGGER.error(elementLocator.toString());
			throw new NoSuchElementException(msgIfFalse);
		}
	}

	/**
	 * Vérifie qu'un élément est bien présent
	 * 
	 * @param elementLocator
	 * @param elementName
	 *            Nom sous lequel l'élément apparaitra dans le log
	 * @param logIfTrue
	 *            True: si l'élément est présent la vérification apparaitra dans
	 *            les log False: si l'élément est présent la vérification
	 *            n'apparaitra pas dans les log
	 */
	public void verifierPresenceElement(By elementLocator, String elementName, boolean logIfTrue) {
		String msgIfTrue = "L'élément " + elementName + " est bien présent.";
		String msgIfFalse = "L'élément " + elementName + " n'est pas présent.";
		verifierPresenceElement(elementLocator, logIfTrue, msgIfTrue, msgIfFalse);
	}

	/**
	 * Vérifie qu'un élément est bien présent. Si l'élément est bien présent, un
	 * message est affiché dans les logs.
	 * 
	 * @param elementLocator
	 * @param msgIfTrue
	 *            Message qui sera affiché si l'élément est bien présent et si logIfTrue a pour valeur True
	 * @param msgIfFalse
	 *            Message qui sera affiché si l'élément est absent
	 */
	public void verifierPresenceElement(By elementLocator, String msgIfTrue, String msgIfFalse) {
		verifierPresenceElement(elementLocator, true, msgIfTrue, msgIfFalse);
	}

	/**
	 * Vérifie qu'un élément est bien présent. Si l'élément est bien présent,
	 * aucun message n'est affiché dans les logs.
	 * 
	 * @param elementLocator
	 * @param msgIfFalse
	 *            Message customisé qui sera affiché si l'élément est absent
	 */
	public void verifierPresenceElement(By elementLocator, String msgIfFalse) {
		verifierPresenceElement(elementLocator, false, null, msgIfFalse);
	}

	/**
	 * Fonction qui permet de au driver de basculer à la nouvelle fenêtre
	 * 
	 * @param driver de la fenêtre courante
	 * @return le driver de la dernière fenêtre ouverte
	 */
	public static WebDriver changerDeFenetre(WebDriver driver) {	

		Iterator<String> i = driver.getWindowHandles().iterator();
		Integer compteur = 0;
		String window = "";
		while(i.hasNext()){
			compteur++;
			window = i.next();
		}
		return  (WebDriver) driver.switchTo().window(window);
		//return  driver.switchTo().window(window);
	}


	// ----------------------------
	// DATE
	// ----------------------------
	/**
	 * Date du jour
	 * @return string date du jour au format JJ/MM/AAAA 
	 */
	public String dateDuJour() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();
		String strDateFmt = dtf.format(localDate);
		LOGGER.debug("Date du jour : " + strDateFmt);
		return strDateFmt;
	}


	/**
	 * Date du lendemain
	 * @return string date du lendemain au format JJ/MM/AAA
	 */
	public String dateDuLendemain() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();
		localDate = localDate.plusDays(1);
		String strDateFmt = dtf.format(localDate);
		LOGGER.debug("Date de demain : " + strDateFmt);
		return strDateFmt;
	}

	/**
	 * Date d'hier 
	 * @return string date d'hier au format JJ/MM/AAAA
	 */
	public String dateDuLaVeille() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();
		localDate = localDate.plusDays(-1);
		String strDateFmt = dtf.format(localDate);
		LOGGER.debug("Date d'hier : " + strDateFmt);
		return strDateFmt;
	}

	/**
	 * Fonction qui renvoie l'annee en cours
	 * 
	 * @return string annee en cours au format AAAA
	 */
	public String anneeDuJour(){
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy");
		String annee=format.format(date);
		LOGGER.debug("Année en cours : " + annee);
		return annee;
	}
	/**
	 * Fonction qui renvoie l'annee suivante
	 * 
	 * @return string annee en cours au format AAAA
	 */
	public String anneeSuivante(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();
		localDate = localDate.plusYears(1);
		String strDateFmt = dtf.format(localDate);
		LOGGER.debug("Date d'hier : " + strDateFmt);;
		return strDateFmt;
	}

	/**
	 * Fonction qui renvoie le mois en cours
	 * 
	 * @return string mois en cours au format MM
	 */
	public String moisDuJour(){
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat format = new SimpleDateFormat("MM");
		String mois=format.format(date);
		LOGGER.debug("Mois en cours : " + mois);
		return mois;
	}
	
	
	
	
	/**
	 * Fonction qui renvoie la date et heure en cours 
	 * @return au foramt AAAA-MM-JJ HH:MM:SS
	 */
	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// dd/MM/yyyy
		Date now = new Date();
		String strDate = sdfDate.format(now);
		LOGGER.debug("Date en cours : " + strDate);
		return strDate;
	}



	/**
	 * retourne le mois en cours et cast la premiere lettre en majuscule
	 * 
	 * @return mois
	 */
	public String getMoisEnCours() {

		Calendar cal = Calendar.getInstance();
		String mois = new SimpleDateFormat("MMMM").format(cal.getTime());
		// mois = mois.substring(0, 1).toUpperCase() + mois.substring(1);
		mois = mois.substring(1);
		return mois;

	}
	/**
	 * retourne le mois Suivant
	 * @return string date du mois suivant  au format JJ/MM/AAA
	 */
	public String MoisSuivant() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();
		localDate = localDate.plusMonths(1);
		String strDateFmt = dtf.format(localDate);
		LOGGER.debug("Date de demain : " + strDateFmt);
		return strDateFmt;
	}
	/**
	 * retourne la date plus le nombre de mois passés en parametres  
	 * @return string date du mois suivant  au format JJ/MM/AAA
	 */
	public String MoisSuivants(int t) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate localDate = LocalDate.now();
		for (int i=0;i<=t;i++){
		localDate = localDate.plusMonths(1);
		}
		String strDateFmt = dtf.format(localDate);
		LOGGER.debug("Date de demain : " + strDateFmt);
		return strDateFmt;
	}

	/**
	 * cette fonction permet de dezoomer une page web de 20%
	 */
	public void dezoomerPageWeb() {
		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
		html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
		html.sendKeys(Keys.chord(Keys.CONTROL, Keys.SUBTRACT));
	}

	/**
	 * cette fonction permet de zoomer une page web de 20%
	 */
	public void zoomerPageWeb() {
		WebElement html = driver.findElement(By.tagName("html"));
		html.sendKeys(Keys.chord(Keys.CONTROL, "0"));
		html.sendKeys(Keys.chord(Keys.CONTROL, Keys.ADD));
		html.sendKeys(Keys.chord(Keys.CONTROL, Keys.ADD));
	}

	/**
	 * Ouvrir un nouvel onglet
	 * 
	 * @param driver2
	 * @param url
	 *            de la page a ouvrir
	 */
	public static void ouvrirNouvelOnglet(WebDriver driver2) {

		try {
			driver2.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
			driver2 =  ConfigConnexion.changerDeFenetre(driver2);
			LOGGER.info("Ouverture d'un nouvel onglet");
		} catch (TimeoutException e) {
			LOGGER.error("Erreur d'ouverture d'un nouvel onglet");
			e.printStackTrace();
		}
	}
	
	public static void ouvrirNouvelOnglet2(WebDriver driver, String url) {

		try {
//			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
//			driver.get(url);
		    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		    ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs.get(1));
		    driver.get(url);
			LOGGER.info("Ouverture d'un nouvel onglet");
		} catch (TimeoutException e) {
			LOGGER.error("Erreur d'ouverture d'un nouvel onglet");
			e.printStackTrace();
		}
	}

	// public static SeleniumDriver changerDeFenetre(SeleniumDriver driver) {
	//
	// Iterator<String> i = driver.getWindowHandles().iterator();
	// Integer compteur = 0;
	// String window = "";
	// while(i.hasNext()){
	// compteur++;
	// window = i.next();
	// }
	// return (SeleniumDriver) driver.switchTo().window(window);
	// }

	public void fermerFenetreCourante(){

		driver.findElement(By.tagName("body")).sendKeys(Keys.CONTROL+"w");	
	}

	
	public void retourFenetreInitiale() {
		//retour à la page initiale
		//String oldTab = driver.getWindowHandle();
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "" + Keys.SHIFT + "" + Keys.TAB);
		//driver.switchTo().window(oldTab);
		driver.switchTo().window(tabs.get(0));
		attendre(2000);
	}

	
	
	
	/**
	 * 
	 * @param element
	 * @return
	 * @deprecated boolean elementPresent(By elementLocator)
	 */
	public boolean isElementVisible(final By element) {

		if (driver.findElements(element).size() > 0) {
			return true;
		} else {
			return false;
		}

	}
    
	/**
     * Recherche un élément dans la page et renvoie True / False 
     * @param elementLocator 
     * @return boolean (element trouvé)
     */
	public boolean elementPresent(By elementLocator) {
		boolean isElementPresent;
		attendre(500);
		//driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		AjaxEnCours();
		if (driver.findElements(elementLocator).size() > 0) {
			LOGGER.debug("Element présent : " + elementLocator.toString());
			isElementPresent =  true;
		} else {
			LOGGER.debug("Element non présent : " + elementLocator.toString());
			isElementPresent = false;
		}
		//driver.manage().timeouts().implicitlyWait(DEFAULT_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS);
		return isElementPresent;
	}
	
	/**
	 * is elementlocator Clickable
	 * @param elementLocator By
	 * @return boolean
	 */
	public boolean isClickable(By elementLocator) {
		boolean isElementClikable;
		LOGGER.debug("isClickable " + elementLocator.toString() + " ?");
		try {
			//WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
			AjaxEnCours();
			WebDriverWait wait = new WebDriverWait(driver, DEFAULT_SHORT_TIME_OUT_IN_SECONDS);
			wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
			isElementClikable = true;
		} catch (Exception e) {
			isElementClikable = false;
		}
		LOGGER.debug("isClickable " + elementLocator.toString() + " : " + isElementClikable);
		return isElementClikable;
	}

	/**
	 * Cette fonction permet de fermer une popup (pas des popup windows)
	 * 
	 * @param fermer
	 */
	public void fermerAlert(boolean fermer) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, DEFAULT_TIME_OUT_IN_SECONDS);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			if (fermer) {
				alert.accept();
			} else {
				alert.dismiss();
			}

		} catch (Exception e) {
			// exception handling
		}
	}

	
	String retourneNomWebElement(WebElement a){		

		String nomBoutonA= a.toString();
		String tb =	nomBoutonA.substring(nomBoutonA.indexOf("id")+4,nomBoutonA.length()-1);
		return tb;
	}

	protected void estAGauche(WebElement b, WebElement c) {
		// TODO Auto-generated method stub
		LOGGER.debug("verifie si un webElement est à  gauche d'un autre  ");

		String NomBoutonA =retourneNomWebElement(b);
		String NomBoutonB =retourneNomWebElement(c);
		int BoutonAPosX =b.getLocation().getX();	 
		int BoutonBPosX=c.getLocation().getX();
		if (BoutonAPosX<BoutonBPosX)
			LOGGER.step("0XX. le bouton " +NomBoutonA+" est bien a gauche  de  "+NomBoutonB , "OK");
		else
			LOGGER.step("0XX. le bouton " +NomBoutonA +" n'est pas a gauche de  "+NomBoutonB, "OK");

	}
	

	public void estAuDessus(WebElement a,WebElement b) throws Exception{
		LOGGER.debug("verifie si un webElement est au dessus  d'un autre  ");

		int BoutonAPosY =a.getLocation().getY();	 
		int BoutonBPosY=b.getLocation().getY();
		if (BoutonAPosY<BoutonBPosY)
			LOGGER.step("0XX. le bouton " +a+ " est bien au dessus  ", "OK");
		else if (BoutonAPosY==BoutonBPosY)
			LOGGER.step("0XX. le bouton "+a+ " est au même niveau  ", "OK");
		else 
			LOGGER.step("00.Le bouton" +a+ " est en dessous   ", "OK");

	}
	
	protected String lienSiteCabine(String valeur){
		return "//li/a[contains(text(),'"+valeur+"')]";
	}
	
	
	/**
	 * Cette fonction permet d'attendre qu'un élément  soit présent
	 * 
	 */
	
	public static boolean waitTillElementisDisplayed(By elementLocator, int timeoutInSeconds)
    {
		boolean elementDisplayed = false;

        for (int i = 0; i < timeoutInSeconds; i++)
        {
            
                if (timeoutInSeconds > 0)
                {
                	WebDriverWait wait = new WebDriverWait(driver,DEFAULT_TIME_OUT_IN_SECONDS);
                   
                }
                elementDisplayed = driver.findElement(elementLocator).isDisplayed();
           
           
        }
        return elementDisplayed;

    }
	
}